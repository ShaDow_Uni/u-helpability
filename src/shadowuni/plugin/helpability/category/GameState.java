package shadowuni.plugin.helpability.category;

public enum GameState {
	WAITING, PREPARING, DRAWING, STARTING, PLAYING, END
	// 대기   게임시작카운트  능력추첨 능력추첨후카운트  게임중  끝
}