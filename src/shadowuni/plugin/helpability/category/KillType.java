package shadowuni.plugin.helpability.category;

public enum KillType {
	NORMAL(""), DOUBLE("더블 킬"), TRIPLE("트리플 킬"), QUADRA("쿼드라 킬"), PENTA("펜타 킬");
	
	private String text = null;
	
	private KillType(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
}