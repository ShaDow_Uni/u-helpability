package shadowuni.plugin.helpability.category;

public enum DeathReason {
	LOW_HEALTH, PLAYER, NATURE
}