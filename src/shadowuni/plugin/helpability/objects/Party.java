package shadowuni.plugin.helpability.objects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Party {
	
	public String name = null;
	public List<String> players = new ArrayList<>();
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void addPlayer(Player p) {
		if(players.contains(p.getName())) return;
		players.add(p.getName());
	}
	
	public void removePlayer(Player p) {
		if(!players.contains(p.getName())) return;
		players.remove(p.getName());
	}
	
	public boolean existPlayer(Player p) {
		return players.contains(p.getName());
	}
	
	public String getName() {
		return name;
	}
	
	public ArrayList<Player> getPlayers() {
		ArrayList<Player> partyplayers = new ArrayList<>();
		for(String pname : players) {
			partyplayers.add(Bukkit.getPlayer(pname));
		}
		return partyplayers;
	}
	
}
