package shadowuni.plugin.helpability.objects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;

public class GameMap {

	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@Setter
	@Getter
	private String name;
	
	@Setter
	@Getter
	private boolean randomTeleport = false;
	
	@Setter
	@Getter
	private Location mapLocation, TPAllLocation;
	@Getter
	private Location minMapLocation, maxMapLocation, minTPAllLocation, maxTPAllLocation;
	
	@Getter
	private List<Location> packetLocations = new ArrayList<>();
	
	public void setMapLimitLocation(Location loc1, Location loc2) {
		minMapLocation = loc1;
		maxMapLocation = loc2;
		replaceMinMax(getMinMapLocation(), getMaxMapLocation());
	}
	
	public void setTPAllLimitLocation(Location loc1, Location loc2) {
		minTPAllLocation = loc1;
		maxTPAllLocation = loc2;
		replaceMinMax(getMinTPAllLocation(), getMaxTPAllLocation());
	}
	
	private void replaceMinMax(Location min, Location max) {
		if(min == null || max == null) return;
		double lx = min.getX();
		double lz = min.getZ();
		double rx = max.getX();
		double rz = max.getZ();
		double temp;
		if(lx > rx) {
			temp = lx;
			lx = rx;
			rx = temp;
		}
		if(lz > rz) {
			temp = lz;
			lz = rz;
			rz = temp;
		}
		min.setX(lx);
		min.setZ(lz);
		max.setX(rx);
		max.setZ(rz);
	}
	
	public void addPacketLocation(Location loc) {
		packetLocations.add(loc);
	}
	
	public Location getRandomLocation() {
		int x = api.getRandomRange((int) getMinMapLocation().getX(), (int) getMaxMapLocation().getX());
		int z = api.getRandomRange((int) getMinMapLocation().getZ(), (int) getMaxMapLocation().getZ());
		Block b = getMapLocation().getWorld().getHighestBlockAt(x, z);
		Location l = b.getLocation();
		l.setY(l.getY() - 1);
		b = b.getWorld().getBlockAt(l);
		if(b == null) return getRandomLocation();
		if(b.getType().equals(Material.WATER) || b.getType().equals(Material.STATIONARY_WATER) ||
				b.getType().equals(Material.LAVA) || b.getType().equals(Material.STATIONARY_LAVA) ||
				b.getType().equals(Material.BEDROCK) || b.getType().equals(Material.AIR)) return getRandomLocation();
		l.setY(l.getY() + 1);
		return l;
	}
	
}