package shadowuni.plugin.helpability.objects;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.category.KillType;

public class GamePlayer {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@Setter
	@Getter
	private String name, partyName, invitedPartyName;
	
	@Setter
	@Getter
	private long quitTime, lastKillTime;
	
	@Setter
	@Getter
	private boolean online, dead, watchMode, partyChat, hide;
	
	@Setter
	@Getter
	private KillType lastKillType = null;
	
	@Setter
	@Getter
	private Location lastLocation = null;	
	
	@Getter
	private HashMap<String, Long> lastHitTime = new HashMap<>();
	
	public void setPlayer(Player p) {
		name = p.getName();
	}
	
	public void setLastHitTime(String name, long hitTime) {
		lastHitTime.put(name.toLowerCase(), hitTime);
	}
	
	public String getDisplayName() {
		if(api.isUseBungeeChat() && api.getChatApi().getChatManager().existDisplayName(getName())) return api.getChatApi().getChatManager().getDisplayName(getName());
		if(getPlayer() == null) return getName();
		return getPlayer().getName();
	}
	
	public Player getPlayer() {
		return Bukkit.getPlayer(name);
	}
	
	public boolean hasParty() {
		return partyName != null;
	}
	
	public HashMap<String, Long> getLastHitTimes() {
		return lastHitTime;
	}
	
	public long getLastHitTime(String name) {
		return lastHitTime.get(name.toLowerCase());
	}
	
	public boolean hidePlayer() {
		if(!isOnline() || isHide()) return false;
		for(Player ap : Bukkit.getOnlinePlayers()) {
			ap.hidePlayer(getPlayer());
		}
		return hide = true;
	}
	
	public boolean showPlayer() {
		if(!isOnline() || !isHide()) return false;
		for(Player ap : Bukkit.getOnlinePlayers()) {
			ap.showPlayer(getPlayer());
		}
		hide = false;
		return true;
	}
	
}