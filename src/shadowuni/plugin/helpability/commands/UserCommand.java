package shadowuni.plugin.helpability.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import Xeon.VisualAbility.MainModule.AbilityBase;
import Xeon.VisualAbility.MajorModule.AbilityList;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.category.DeathReason;
import shadowuni.plugin.helpability.category.GameState;
import shadowuni.plugin.helpability.events.DeathEvent;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class UserCommand implements CommandInterface {
	
	private HelpAbilityAPI api = HelpAbility.getApi();

	@Override
	public boolean onCommand(Player p, String[] args) {
		if(args[0].equals("스폰") || args[0].equalsIgnoreCase("spawn")) {
			if(api.getGameManager().isGameStarted()) {
				api.msg(p, ChatColor.RED, "게임 중에는 스폰으로 이동할 수 없습니다!");
				return true;
			} else if(api.getMapManager().getSpawnMap() == null) {
				api.msg(p, ChatColor.RED, "스폰이 설정되지 않았습니다!");
				return true;
			}
			if(p.getVehicle() != null) {
				p.leaveVehicle();
			}
			p.teleport(api.getMapManager().getSpawnMap().getMapLocation());
			api.msg(p, ChatColor.DARK_AQUA, "스폰으로 이동되었습니다.");
			return true;
		} else if(args[0].equals("목록") || args[0].equalsIgnoreCase("list")) {
			api.msg(p, ChatColor.YELLOW, "게임에 참여 중인 " + ChatColor.WHITE + "플레이어[" + api.getPlayerManager().getGamePlayers().size() + "]: " + api.buildGamePlayerList(api.getPlayerManager().getGamePlayers()));
			if(api.getGameManager().isGameStarted() && api.getPlayerManager().getDeadPlayers().size() > 0) {
				api.msg(p, ChatColor.YELLOW, "탈락한 플레이어[" + api.getPlayerManager().getDeadPlayers().size() + "]: " + api.buildGamePlayerList(api.getPlayerManager().getDeadPlayers()));
			}
			if(api.getPlayerManager().getWatchPlayers().size() > 0) {
				api.msg(p, ChatColor.YELLOW, "관전 중인 플레이어[" + api.getPlayerManager().getWatchPlayers().size() + "]: " + api.buildGamePlayerList(api.getPlayerManager().getWatchPlayers()));
			}
			return true;
		} else if(args[0].equals("맵목록") || args[0].equalsIgnoreCase("mapList")) {
			if(api.getMapManager().getMaps().size() < 1) {
				api.msg(p, ChatColor.RED, "아직 설정된 맵이 없습니다!");
				return true;
			}
			api.msg(p, ChatColor.YELLOW, "맵 목록: " + api.buildStringList(api.getMapManager().getMaps().keySet()));
			return true;
		} else if(args[0].equals("플레이시간") || args[0].equalsIgnoreCase("playTime")) {
			if(!api.getGameManager().isGameStarted()) {
				api.msg(p, ChatColor.RED, "아직 게임이 시작되지 않았습니다!");
				return true;
			}
			api.msg(p, ChatColor.YELLOW, "플레이 시간: " + api.buildTime((int) (api.getGameManager().getPlayTime() / 1000)));
			return true;
		} else if(args[0].equals("시작투표") || args[0].equalsIgnoreCase("startVote")) {
			if(api.getGameManager().isGameStarted()) {
				api.msg(p, ChatColor.RED, "이미 게임이 시작되었습니다!");
				return true;
			} else if(api.getVoteManager().isVoting()) {
				api.msg(p, ChatColor.RED, "이미 투표를 진행 중입니다.");
				return true;
			} else if(api.getPlayerManager().getGamePlayers().size() < 2 || (api.isUseBungeeParty() && api.getPartyApi().getPartyManager().getPartys().size() == 1 && api.getPartyApi().getPartyManager().getPartys().values().iterator().next().getOnlinePlayers().size() == api.getPlayerManager().getGamePlayers().size())) {
				api.msg(p, ChatColor.RED, "인원이 적어 투표를 진행할 수 없습니다!");
				return true;
			}
			api.getVoteManager().startVote(api.getVoteCount());
			api.broadcast(ChatColor.DARK_AQUA, "게임 시작 투표가 시작되었습니다.");
			api.broadcast(ChatColor.DARK_AQUA, "'/ha 찬성', '/ha 반대' 명령어를 사용하여 투표에 참여하세요!");
			return true;
		} else if(args[0].equals("찬성") || args[0].equalsIgnoreCase("agree")) {
			if(api.getGameManager().isGameStarted()) {
				api.msg(p, ChatColor.RED, "이미 게임이 시작되었습니다!");
				return true;
			} else if(!api.getVoteManager().isVoting()) {
				api.msg(p, ChatColor.RED, "투표 중이 아닙니다!");
				return true;
			} else if(api.getVoteManager().isVoted(p)) {
				api.msg(p, ChatColor.RED, "이미 투표에 참여했습니다!");
				return true;
			} else if(api.getPlayerManager().getGamePlayers().size() < 2) {
				api.msg(p, ChatColor.RED, "인원이 부족하여 투표를 진행할 수 없습니다!");
				api.broadcast(ChatColor.GREEN, "인원이 부족하여 투표가 중단되었습니다!");
				api.getVoteManager().stopVote();
				return true;
			}
			api.getVoteManager().joinVote(p, true);
			api.msg(p, ChatColor.DARK_AQUA, "투표에 찬성했습니다!");
			return true;
		} else if(args[0].equals("반대") || args[0].equalsIgnoreCase("disagree")) {
			if(api.getGameManager().isGameStarted()) {
				api.msg(p, ChatColor.RED, "이미 게임이 시작되었습니다!");
				return true;
			} else if(!api.getVoteManager().isVoting()) {
				api.msg(p, ChatColor.RED, "투표 중이 아닙니다!");
				return true;
			} else if(api.getVoteManager().isVoted(p)) {
				api.msg(p, ChatColor.RED, "이미 투표에 참여했습니다!");
				return true;
			} else if(api.getPlayerManager().getGamePlayers().size() < 2) {
				api.msg(p, ChatColor.RED, "인원이 적어 투표를 진행할 수 없습니다!");
				api.broadcast(ChatColor.GREEN, "인원이 부족하여 투표가 중단되었습니다!");
				api.getVoteManager().stopVote();
				return true;
			}
			api.getVoteManager().joinVote(p, false);
			api.msg(p, ChatColor.DARK_AQUA, "투표에 반대했습니다!");
			return true;
		} else if(args[0].equals("관전") || args[0].equalsIgnoreCase("watchMode")) {
			GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
			if(gp.isWatchMode()) {
				if(api.getGameManager().isGameStarted()) {
					api.msg(p, ChatColor.RED, "게임 중에는 관전 모드를 해제할 수 없습니다!");
					return true;
				}
				gp.setWatchMode(false);
				gp.showPlayer();
				p.setAllowFlight(false);
				p.setFlying(false);
				p.setGameMode(GameMode.SURVIVAL);
				api.msg(p, ChatColor.BLUE, "관전 모드가 해제되었습니다!");
				return true;
			} else if(!api.getGameManager().isGameStarted()) {
				api.msg(p, ChatColor.RED, "게임 시작 전에는 관전 모드를 사용할 수 없습니다!");
				return true;
			} else if(api.isUseBitAbility()) {
				for(AbilityBase ab : AbilityList.AbilityList) {
					if(!ab.PlayerCheck(p)) continue;
					ab.SetPlayer(null, false);
				}
			} else if(api.isUseDyeAbility()) {
				for(Physical.Fighters.MainModule.AbilityBase ab : Physical.Fighters.MajorModule.AbilityList.AbilityList) {
					if(!ab.PlayerCheck(p)) continue;
					ab.SetPlayer(null, false);
				}
			}
			if(!api.getGameManager().getGameState().equals(GameState.END) && !gp.isDead() && p.getHealth() > 0 && p.getHealth() < api.getQuitDeathHealth()) {
				p.setHealth(0);
				Bukkit.getPluginManager().callEvent(new DeathEvent(p, p.getKiller(), DeathReason.LOW_HEALTH, null));
				api.broadcast(ChatColor.DARK_GREEN, gp.getDisplayName() + "님께서 낮은 체력으로 관전모드로 전환하여 탈락 처리되었습니다.");
			}
			gp.setWatchMode(true);
			gp.hidePlayer();
			p.setGameMode(GameMode.ADVENTURE);
			p.setAllowFlight(true);
			p.setFlying(true);
			api.msg(p, ChatColor.BLUE, "관전 모드로 전환되었습니다!");
			api.broadcast(ChatColor.BLUE, "'" + gp.getDisplayName() + "' 님께서 관전 모드로 전환하셨습니다! (관전자 수: " + api.getPlayerManager().getWatchPlayers().size() + ")");
			if(api.getGameManager().finish()) {
				api.shutdown(13);
			} else if(!api.getGameManager().getGameState().equals(GameState.END) && api.getPlayerManager().getGamePlayers().size() < 2) {
				for(Player ap : Bukkit.getOnlinePlayers()) {
					ap.teleport(api.getMapManager().getSpawnMap().getMapLocation());
				}
				api.getGameManager().GameStop(null);
				api.broadcast(ChatColor.DARK_GREEN, "인원 수가 부족하여 게임이 중단됩니다!");
			}
			return true;
		}
		return false;
	}
	
}
