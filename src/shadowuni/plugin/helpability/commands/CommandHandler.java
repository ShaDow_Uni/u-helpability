package shadowuni.plugin.helpability.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;

public class CommandHandler implements CommandExecutor {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	private List<CommandInterface> cmdList = new ArrayList<>();
	
	public CommandHandler() {
		registerCommand(new UserCommand());
		if(api.isUseParty()) {
			registerCommand(new PartyCommand());
		}
		registerCommand(new KitCommand());
		registerCommand(new RankItemCommand());
		registerCommand(new SupplyCommand());
		registerCommand(new AdminCommand());
		// debug
	}
	
	public void registerCommand(CommandInterface ci) {
		cmdList.add(ci);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if(!(sender instanceof Player)) {
			api.log("콘솔에서는 사용이 불가능합니다.");
			return true;
		}
		
		Player p = (Player) sender;
		
		if(args.length < 1) {
			sendHelpMessage(p);
			return true;
		}
		
		int n = 0;
		for(CommandInterface command : cmdList) {
			if(!command.onCommand(p, args)) n++;
		}
		if(n == cmdList.size()) {
			sendHelpMessage(p);
		}
		
		return true;
	}
	
	public void sendHelpMessage(Player p) {
		api.msg(p, ChatColor.DARK_GREEN, ChatColor.DARK_GREEN + "U-HelpAbility");
		api.msg(p, ChatColor.DARK_GREEN, ChatColor.DARK_GREEN + "제작자: " + ChatColor.WHITE + "ShaDow_Uni (셰도우 우니)");
		api.msg(p, ChatColor.DARK_GREEN, ChatColor.DARK_GREEN + "버전: " + ChatColor.WHITE + HelpAbility.getVersion());
		api.msg(p, ChatColor.DARK_GREEN, "/ha 스폰" + ChatColor.GREEN + " - 스폰으로 이동합니다.");
		api.msg(p, ChatColor.DARK_GREEN, "/ha 목록" + ChatColor.GREEN + " - 플레이어 목록을 확인합니다.");
		api.msg(p, ChatColor.DARK_GREEN, "/ha 플레이시간" + ChatColor.GREEN + " - 게임 플레이 시간을 확인합니다.");
		api.msg(p, ChatColor.DARK_GREEN, "/ha 시작투표" + ChatColor.GREEN + " - 게임 시작 투표를 시작합니다.");
		api.msg(p, ChatColor.DARK_GREEN, "/ha 찬성" + ChatColor.GREEN + " - 게임 시작 투표에 찬성합니다.");
		api.msg(p, ChatColor.DARK_GREEN, "/ha 반대" + ChatColor.GREEN + " - 게임 시작 투표에 반대합니다.");
		if(api.isUseParty()) {
			api.msg(p, ChatColor.DARK_GREEN, "/ha 파티" + ChatColor.GREEN + " * 파티 관련 명령어를 확인합니다.");
		}
		if(api.isAdmin(p, false)) {
			api.msg(p, ChatColor.BLUE, "/ha 강제시작" + ChatColor.DARK_AQUA + " - 게임을 강제로 시작시킵니다.");
			api.msg(p, ChatColor.BLUE, "/ha 강제중지" + ChatColor.DARK_AQUA + " - 게임을 강제로 중지시킵니다.");
			api.msg(p, ChatColor.BLUE, "/ha 투표현황" + ChatColor.DARK_AQUA + " - 투표 현황을 확인합니다.");
			api.msg(p, ChatColor.BLUE, "/ha 스폰설정" + ChatColor.DARK_AQUA + " - 자신의 위치를 스폰으로 설정합니다.");
			api.msg(p, ChatColor.BLUE, "/ha 맵위치설정 <맵 이름>" + ChatColor.DARK_AQUA + " - 자신의 위치를 지정한 맵의 스폰으로 설정합니다.");
			api.msg(p, ChatColor.BLUE, "/ha 텔레포트장소설정 <맵 이름>" + ChatColor.DARK_AQUA + " - 자신의 위치를 지정한 맵의 텔레포트 장소로 설정합니다.");
			api.msg(p, ChatColor.BLUE, "/ha 게임맵설정 <맵 이름>" + ChatColor.DARK_AQUA + " - 플레이 할 맵을 지정한 맵으로 설정합니다.");
			api.msg(p, ChatColor.BLUE, "/ha 맵이동 <맵 이름>" + ChatColor.DARK_AQUA + " - 지정한 맵으로 이동합니다.");
			api.msg(p, ChatColor.BLUE, "/ha 텔레포트장소이동 <맵 이름>" + ChatColor.DARK_AQUA + " - 지정한 맵의 텔레포트장소로 이동합니다.");
			api.msg(p, ChatColor.BLUE, "/ha 맵삭제 <맵 이름>" + ChatColor.DARK_AQUA + " - 지정한 맵을 삭제합니다.");
			api.msg(p, ChatColor.BLUE, "/ha 맵범위설정 <맵 이름>" + ChatColor.DARK_AQUA + " - 맵의 범위를 설정합니다.");
			api.msg(p, ChatColor.BLUE, "/ha 랜덤스폰설정 <맵 이름>" + ChatColor.DARK_AQUA + " - 맵의 랜덤 스폰을 설정하거나 해제합니다.");
			api.msg(p, ChatColor.BLUE, "/ha 플레이어목록" + ChatColor.DARK_AQUA + " - 플레이어 목록을 확인합니다.");
			api.msg(p, ChatColor.BLUE, "/ha 킷" + ChatColor.DARK_AQUA + " * 킷 관련 명령어를 확인합니다.");
			api.msg(p, ChatColor.BLUE, "/ha 등급아이템" + ChatColor.DARK_AQUA + " * 등급 아이템 관련 명령어를 확인합니다.");
			api.msg(p, ChatColor.BLUE, "/ha 보급품" + ChatColor.DARK_AQUA + " * 보급품 관련 명령어를 확인합니다.");
		}
	}

}
