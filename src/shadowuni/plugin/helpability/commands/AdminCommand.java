package shadowuni.plugin.helpability.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.objects.GameMap;

public class AdminCommand implements CommandInterface {

	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@Override
	public boolean onCommand(Player p, String[] args) {
		if(args[0].equals("스폰설정") || args[0].equalsIgnoreCase("setSpawn")) {
			if(!api.isAdmin(p, true)) return true;
			api.getMapManager().setSpawnLocation(p.getLocation());
			api.msg(p, ChatColor.BLUE, "스폰으로 설정되었습니다.");
			api.getFileManager().saveSpawn();
			return true;
		} else if(args[0].equals("맵위치설정") || args[0].equalsIgnoreCase("setMapLocation")) {
			if(!api.isAdmin(p, true)) return true;
			if(args.length < 2) {
				api.msg(p, ChatColor.YELLOW, "사용법: /ha 맵위치설정 <맵 이름>");
				return true;
			}
			GameMap map = new GameMap();
			if(api.getMapManager().existMap(args[1])) {
				map = api.getMapManager().getMap(args[1]);
			}
			map.setName(args[1]);
			map.setMapLocation(p.getLocation());
			api.getMapManager().setMap(args[1], map);
			api.getFileManager().saveMap(map);
			api.msg(p, ChatColor.BLUE, "'" + map.getName() + "' 맵의 위치가 설정되었습니다!");
			return true;
		} else if(args[0].equals("텔레포트장소설정") || args[0].equalsIgnoreCase("setTpallLocation")) {
			if(!api.isAdmin(p, true)) return true;
			if(args.length < 2) {
				api.msg(p, ChatColor.YELLOW, "사용법: /ha 텔레포트장소설정 <맵 이름>");
				return true;
			} else if(!api.getMapManager().existMap(args[1])) {
				api.msg(p, ChatColor.RED, "존재하지 않는 맵입니다!");
				return true;
			}
			GameMap map = api.getMapManager().getMap(args[1]);
			map.setTPAllLocation(p.getLocation());
			api.getFileManager().saveMap(map);
			api.msg(p, ChatColor.BLUE, "'" + map.getName() + "' 맵의 텔레포트 위치가 설정되었습니다!");
			return true;
		} else if(args[0].equals("게임맵설정") || args[0].equalsIgnoreCase("setMap")) {
			if(!api.isAdmin(p, true)) return true;
			if(args.length < 2) {
				api.msg(p, ChatColor.YELLOW, "사용법: /ha 게임맵설정 <맵 이름>");
				return true;
			} else if(!api.getMapManager().existMap(args[1])) {
				api.msg(p, ChatColor.RED, "존재하지 않는 맵입니다!");
				return true;
			}
			api.getMapManager().setPlayingMap(api.getMapManager().getMap(args[1]));
			api.broadcast(ChatColor.BLUE, "관리자에 의해 맵이 '" + api.getMapManager().getPlayingMap().getName() + "' 맵으로 설정되었습니다!");
			return true;
		} else if(args[0].equals("맵이동") || args[0].equalsIgnoreCase("tpToMap")) {
			if(!api.isAdmin(p, true)) return true;
			if(args.length < 2) {
				api.msg(p, ChatColor.YELLOW, "사용법: /ha 맵이동 <맵 이름>");
				return true;
			} else if(!api.getMapManager().existMap(args[1])) {
				api.msg(p, ChatColor.RED, "존재하지 않는 맵입니다!");
				return true;
			}
			GameMap map = api.getMapManager().getMap(args[1]);
			p.teleport(map.getMapLocation());
			api.msg(p, ChatColor.BLUE, "'" + map.getName() + "' 맵으로 이동되었습니다.");
			return true;
		} else if(args[0].equals("텔레포트장소이동") || args[0].equalsIgnoreCase("tpToTpall")) {
			if(!api.isAdmin(p, true)) return true;
			if(args.length < 2) {
				api.msg(p, ChatColor.YELLOW, "사용법: /ha 텔레포트장소이동 <맵 이름>");
				return true;
			} else if(!api.getMapManager().existMap(args[1])) {
				api.msg(p, ChatColor.RED, "존재하지 않는 맵입니다!");
				return true;
			}
			GameMap map = api.getMapManager().getMap(args[1]);
			p.teleport(map.getTPAllLocation());
			api.msg(p, ChatColor.BLUE, "'" + map.getName() + "' 맵의 텔레포트 장소로 이동되었습니다.");
			return true;
		} else if(args[0].equals("맵삭제") || args[0].equalsIgnoreCase("deleteMap")) {
			if(!api.isAdmin(p, true)) return true;
			if(args.length < 2) {
				api.msg(p, ChatColor.YELLOW, "사용법: /ha 맵삭제 <맵 이름>");
				return true;
			} else if(!api.getMapManager().existMap(args[1])) {
				api.msg(p, ChatColor.RED, "존재하지 않는 맵입니다!");
				return true;
			}
			GameMap map = api.getMapManager().getMap(args[1]);
			api.getMapManager().deleteMap(map);
			api.getFileManager().deleteMap(map);
			api.msg(p, ChatColor.BLUE, "'" + map.getName() + "' 맵을 삭제했습니다!");
			return true;
		} else if(args[0].equals("강제시작") || args[0].equalsIgnoreCase("start")) {
			if(!api.isAdmin(p, true)) return true;
			if(api.getGameManager().isGameStarted()) {
				api.msg(p, ChatColor.RED, "이미 게임이 시작되었습니다!");
				return true;
			}
			api.getGameManager().GameStart(p);
			api.broadcast(ChatColor.BLUE, "'" + p.getName() + "' 님께서 게임을 강제로 시작시켰습니다!");
			return true;
		} else if(args[0].equals("강제중지") || args[0].equalsIgnoreCase("stop")) {
			if(!api.isAdmin(p, true)) return true;
			if(!api.getGameManager().isGameStarted()) {
				api.msg(p, ChatColor.RED, "게임 중이 아닙니다!");
				return true;
			}
			api.getGameManager().GameStop(p);
			for(Player ap : Bukkit.getOnlinePlayers()) {
				ap.teleport(api.getMapManager().getSpawnMap().getMapLocation());
			}
			api.broadcast(ChatColor.BLUE, "'" + p.getName() + "' 님께서 게임을 강제로 중지시켰습니다!");
			return true;
		} else if(args[0].equals("투표현황") || args[0].equalsIgnoreCase("voteInfo")) {
			if(!api.isAdmin(p, true)) return true;
			if(!api.getVoteManager().isVoting()) {
				api.msg(p, ChatColor.RED, "투표 중이 아닙니다!");
				return true;
			}
			String agree = null, disagree = null;
			if(api.getVoteManager().getAgree().size() < 1) {
				agree = "없음";
			} else {
				agree = api.buildStringList(api.getVoteManager().getAgree());
			}
			if(api.getVoteManager().getDisagree().size() < 1) {
				disagree = "없음";
			} else {
				disagree = api.buildStringList(api.getVoteManager().getDisagree());
			}
			api.msg(p, ChatColor.BLUE, "투표 현황");
			api.msg(p, ChatColor.BLUE, "찬성[" + api.getVoteManager().getAgree().size() + "] : " + agree);
			api.msg(p, ChatColor.BLUE, "반대[" + api.getVoteManager().getDisagree().size() + "] : " + disagree);
			return true;
		} else if(args[0].equals("투표중단") || args[0].equalsIgnoreCase("stopVote")) {
			if(!api.isAdmin(p, true)) return true;
			if(!api.getVoteManager().isVoting()) {
				api.msg(p, ChatColor.RED, "투표 중이 아닙니다!");
				return true;
			}
			api.getVoteManager().stopVote();
			api.broadcast(ChatColor.BLUE, "'" + p.getName() + "' 님께서 투표가 중지시켰습니다!");
			return true;
		} else if(args[0].equals("랜덤스폰설정") || args[0].equalsIgnoreCase("setRandomSpawn")) {
			if(!api.isAdmin(p, true)) return true;
			if(args.length < 2) {
				api.msg(p, ChatColor.YELLOW, "사용법: /ha 랜덤스폰설정 <맵>");
				return true;
			}
			GameMap map = api.getMapManager().getMap(args[1]);
			if(map == null) {
				api.msg(p, ChatColor.RED, "존재하지 않는 맵입니다!");
				return true;
			}
			if(map.isRandomTeleport()) {
				map.setRandomTeleport(false);
				api.msg(p, ChatColor.BLUE, "'" + map.getName() + "' 맵의 랜덤 텔레포트를 해제했습니다!");
			} else {
				map.setRandomTeleport(true);
				api.msg(p, ChatColor.BLUE, "'" + map.getName() + "' 맵에 랜덤 텔레포트를 설정했습니다!");
			}
			api.getFileManager().saveMap(map);
			return true;
		} else if(args[0].equals("맵범위설정") || args[0].equalsIgnoreCase("setMapRange")) {
			if(!api.isAdmin(p, true)) return true;
			if(args.length < 2) {
				api.msg(p, ChatColor.YELLOW, "사용법: /ha 맵범위설정 <맵>");
				return true;
			} else if(!api.getMapManager().existLeftLocation(p)|| !api.getMapManager().existRightLocation(p)) {
				api.msg(p, ChatColor.RED, "아직 범위를 설정하지 않았습니다!");
				return true;
			}
			GameMap map = api.getMapManager().getMap(args[1]);
			if(map == null) {
				api.msg(p, ChatColor.RED, "존재하지 않는 맵입니다!");
				return true;
			}
			map.setMapLimitLocation(api.getMapManager().getLeftLocation(p), api.getMapManager().getRightLocation(p));
			api.msg(p, ChatColor.BLUE, "'" + map.getName() + "' 맵의 범위를 설정했습니다!");
			api.getFileManager().saveMap(map);
			return true;
		} else if(args[0].contentEquals("플레이어목록") || args[0].equalsIgnoreCase("alist")) {
			if(!api.isAdmin(p, true)) return true;
			api.msg(p, ChatColor.BLUE, "게임에 참여 중인 온라인 플레이어[" + api.getPlayerManager().getGamePlayers().size() + "]: " + api.buildGamePlayerList(api.getPlayerManager().getGamePlayers()));
			api.msg(p, ChatColor.BLUE, "게임에 참여 중인 모든 플레이어[" + api.getPlayerManager().getAllGamePlayers().size() + "]: " + api.buildGamePlayerList(api.getPlayerManager().getAllGamePlayers()));
			api.msg(p, ChatColor.BLUE, "관전 중인 온라인 플레이어[" + api.getPlayerManager().getWatchPlayers().size() + "]: " + api.buildGamePlayerList(api.getPlayerManager().getWatchPlayers()));
			api.msg(p, ChatColor.BLUE, "모든 관전 중인 플레이어[" + api.getPlayerManager().getAllWatchPlayers().size() + "]: "+ api.buildGamePlayerList(api.getPlayerManager().getAllWatchPlayers()));
			api.msg(p, ChatColor.BLUE, "탈락한 플레이어[" + api.getPlayerManager().getDeadPlayers().size() + "]: "+ api.buildGamePlayerList(api.getPlayerManager().getDeadPlayers()));
			api.msg(p, ChatColor.BLUE, "전체 플레이어[" + api.getPlayerManager().getPlayers().size() + "]: "+ api.buildGamePlayerList(api.getPlayerManager().getAllPlayers()));
			api.msg(p, ChatColor.BLUE, "players key[" + api.getPlayerManager().getPlayers().size() + "]: " + api.buildStringList(api.getPlayerManager().getPlayers().keySet()));
			return true;
		}
		return false;
	}
	
}
