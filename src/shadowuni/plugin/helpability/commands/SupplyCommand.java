package shadowuni.plugin.helpability.commands;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.objects.GameMap;

public class SupplyCommand implements CommandInterface {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@Override
	public boolean onCommand(Player p, String[] args) {
		if(!(args[0].equals("보급품") || args[0].equalsIgnoreCase("supply"))) return false;
		if(!api.isAdmin(p, true)) return true;
		if(args.length < 2) {
			sendHelpMessage(p);
			return true;
		} else if(args[1].equals("생성") || args[0].equalsIgnoreCase("create")) {
			ArrayList<ItemStack> items = new ArrayList<>();
			if(args.length > 2) {
				items = api.getSupplyManager().getSupply(args[2]);
				if(items == null) {
					api.msg(p, ChatColor.RED, "존재하지 않는 보급품입니다!");
					return true;
				}
			} else {
				items = api.getSupplyManager().getRandomSupply();
			}
			Location location = p.getLocation();
			api.getSupplyManager().createSupply(location, items);
			api.broadcast(ChatColor.DARK_GREEN, "(X: " + (int) location.getX() + ", Y: " + (int) location.getY() + ", Z: " + (int) location.getZ() + ") 에 보급품이 생성되었습니다.");
			return true;
		} else if(args[1].equals("랜덤생성") || args[0].equalsIgnoreCase("randomCreate")) {
			GameMap map = api.getMapManager().getPlayingMap();
			if(map == null) {
				api.msg(p, ChatColor.RED, "아직 게임이 시작되지 않았습니다!");
				return true;
			}
			ArrayList<ItemStack> items = new ArrayList<>();
			if(args.length > 2) {
				items = api.getSupplyManager().getSupply(args[2]);
				if(items == null) {
					api.msg(p, ChatColor.RED, "존재하지 않는 보급품입니다!");
					return true;
				}
			} else {
				items = api.getSupplyManager().getRandomSupply();
			}
			Location location = api.getSupplyManager().createSupplyAtRandomLocation(map, items);
			location.getWorld().loadChunk(location.getWorld().getChunkAt(location));
			api.broadcast(ChatColor.DARK_GREEN, "(X: " + (int) location.getX() + ", Y: " + (int) location.getY() + ", Z: " + (int) location.getZ() + ") 에 보급품이 생성되었습니다.");
			return true;
		} else if(args[1].equals("목록") || args[0].equalsIgnoreCase("list")) {
			if(api.getSupplyManager().getSupplies().size() < 1) {
				api.msg(p, ChatColor.RED, "등록된 보급품이 없습니다!");
				return true;
			}
			api.msg(p, ChatColor.BLUE, "보급품 목록: " + api.buildStringList(api.getSupplyManager().getSupplies().keySet()));
			return true;
		}
		return false;
	}
	
	public void sendHelpMessage(Player p) {
		api.msg(p, ChatColor.BLUE, ChatColor.DARK_AQUA + "U-HelpAbility - Supply");
		api.msg(p, ChatColor.DARK_GREEN, ChatColor.DARK_AQUA + "제작자: " + ChatColor.WHITE + "ShaDow_Uni (셰도우 우니)");
		api.msg(p, ChatColor.DARK_GREEN, ChatColor.DARK_AQUA + "버전: " + ChatColor.WHITE + HelpAbility.getVersion());
		api.msg(p, ChatColor.BLUE, "/ha 보급품 생성 (<보급품>)" + ChatColor.DARK_AQUA + " - 자신의 위치에 보급품을 생성합니다.");
		api.msg(p, ChatColor.BLUE, "/ha 보급품 랜덤생성 (<보급품>)" + ChatColor.DARK_AQUA + " - 랜덤 위치에 보급품을 생성합니다.");
		api.msg(p, ChatColor.BLUE, "/ha 보급품 목록" + ChatColor.DARK_AQUA + " - 보급품 목록을 확인합니다.");
	}

}
