package shadowuni.plugin.helpability.commands;

import org.bukkit.entity.Player;

public interface CommandInterface {
	public boolean onCommand(Player p, String[] args);
}
