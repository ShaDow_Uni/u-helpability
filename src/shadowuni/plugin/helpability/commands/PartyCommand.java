package shadowuni.plugin.helpability.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.objects.GamePlayer;
import shadowuni.plugin.helpability.objects.Party;

public class PartyCommand implements CommandInterface {
	
	private HelpAbilityAPI api = HelpAbility.getApi();

	@Override
	public boolean onCommand(Player p, String[] args) {
		if(!(args[0].equals("파티") || args[0].equalsIgnoreCase("party"))) return false;
		if(api.isUseBungeeParty()) {
			api.msg(p, ChatColor.RED, "/파티 명령어를 사용해주세요.");
			return true;
		} else if(args.length < 2) {
			sendHelpMessage(p);
			return true;
		} else if(args[1].equals("생성") || args[0].equalsIgnoreCase("create")) {
			if(args.length < 3) {
				api.msg(p, ChatColor.YELLOW, "사용법: /ha 파티 생성 <파티이름>");
				return true;
			} else if(api.getPlayerManager().getGamePlayer(p).hasParty()) {
				api.msg(p, ChatColor.RED, "이미 파티에 참여 중입니다!");
				return true;
			} else if(api.getPartyManager().existParty(args[2])) {
				api.msg(p, ChatColor.RED, "이미 존재하는 파티 이름입니다!");
				return true;
			}
			GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
			gp.setPartyName(args[2]);
			Party party = api.getPartyManager().createParty(args[2]);
			party.addPlayer(p);
			api.msg(p, ChatColor.GREEN, "'" + args[2] + "' 파티가 생성되었습니다!");
			return true;
		} else if(args[1].equals("초대") || args[0].equalsIgnoreCase("invite")) {
			if(args.length < 3) {
				api.msg(p, ChatColor.YELLOW, "사용법: /ha 파티 초대 <플레이어>");
				return true;
			} else if(!api.getPlayerManager().getGamePlayer(p).hasParty()) {
				api.msg(p, ChatColor.RED, "참여 중인 파티가 없습니다!");
				return true;
			}
			GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
			Party party = api.getPartyManager().getParty(gp.getPartyName());
			if(party.getPlayers().size() >= api.getMaxPartyPlayerCount()) {
				api.msg(p, ChatColor.RED, "파티가 가득 찼습니다!");
				return true;
			}
			Player target = Bukkit.getPlayer(args[2]);
			if(target == null) {
				api.msg(p, ChatColor.RED, "접속중이 아닌 플레이어입니다!");
				return true;
			}
			GamePlayer tp = api.getPlayerManager().getGamePlayer(target);
			if(tp.hasParty()) {
				api.msg(p, ChatColor.RED, "이미 다른 파티에 참여 중인 플레이어입니다!");
				return true;
			}
			tp.setInvitedPartyName(party.getName());
			api.msg(p, ChatColor.GREEN, "'" + target.getName() + "' 님을 파티에 초대했습니다!");
			api.msg(target, ChatColor.GREEN, "'" + p.getName() + "' 님께서 '" + party.getName() + "' 파티에 초대했습니다! (/ha 파티 수락 | /ha 파티 거절)");
			return true;
		} else if(args[1].equals("수락") || args[0].equalsIgnoreCase("accept")) {
			GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
			if(gp.hasParty()) {
				api.msg(p, ChatColor.RED, "이미 파티에 가입되어있습니다!");
				return true;
			} else if(gp.getInvitedPartyName() == null) {
				api.msg(p, ChatColor.RED, "아직 파티에 초대받지 못했습니다!");
				return true;
			}
			Party party = api.getPartyManager().getParty(gp.getInvitedPartyName());
			if(party.getPlayers().size() >= api.getMaxPartyPlayerCount()) {
				api.msg(p, ChatColor.RED, "파티가 가득 찼습니다!");
				return true;
			}
			gp.setPartyName(party.getName());
			gp.setInvitedPartyName(null);
			party.addPlayer(p);
			api.msg(p, ChatColor.GREEN, "'" + party.getName() + "' 파티에 참여했습니다!");
			for(Player pp : party.getPlayers()) {
				if(pp == null || pp.equals(p)) continue;
				api.msg(pp, ChatColor.GREEN, "'" + p.getName() + "' 님께서 파티에 참여했습니다!");
			}
			return true;
		} else if(args[1].equals("거절") || args[0].equalsIgnoreCase("refuse")) {
			GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
			if(gp.hasParty()) {
				api.msg(p, ChatColor.RED, "이미 파티에 가입되어있습니다!");
				return true;
			} else if(gp.getInvitedPartyName() == null) {
				api.msg(p, ChatColor.RED, "아직 파티에 초대받지 못했습니다!");
				return true;
			}
			Party party = api.getPartyManager().getParty(gp.getInvitedPartyName());
			gp.setInvitedPartyName(null);
			api.msg(p, ChatColor.GREEN, "'" + party.getName() + "' 파티의 초대를 거절했습니다!");
			for(Player pp : party.getPlayers()) {
				if(pp == null) continue;
				api.msg(pp, ChatColor.GREEN, "'" + p.getName() + "' 님께서 파티 초대를 거절했습니다!");
			}
			return true;
		} else if(args[1].equals("탈퇴") || args[0].equalsIgnoreCase("leave")) {
			GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
			if(!gp.hasParty()) {
				api.msg(p, ChatColor.RED, "참여 중인 파티가 없습니다!");
				return true;
			}
			Party party = api.getPartyManager().getParty(gp.getPartyName());
			gp.setPartyName(null);
			party.removePlayer(p);
			if(party.getPlayers().size() < 1) {
				api.getPartyManager().removeParty(party.getName());
			}
			api.msg(p, ChatColor.GREEN, "'" + party.getName() + "' 파티를 탈퇴했습니다!");
			for(Player pp : party.getPlayers()) {
				if(pp == null) continue;
				api.msg(pp, ChatColor.GREEN, "'" + p.getName() + "' 님께서 파티에서 탈퇴했습니다!");
			}
			return true;
		} else if(args[1].equals("채팅") || args[0].equalsIgnoreCase("chat")) {
			GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
			if(!gp.hasParty()) {
				api.msg(p, ChatColor.RED, "참여 중인 파티가 없습니다!");
				return true;
			} else if(gp.isPartyChat()) {
				gp.setPartyChat(false);
				api.msg(p, ChatColor.GREEN, "파티 채팅 모드가 해제되었습니다!");
				return true;
			}
			gp.setPartyChat(true);
			api.msg(p, ChatColor.GREEN, "파티 채팅 모드로 전환되었습니다!");
			return true;
		} else if(args[1].equals("정보") || args[0].equalsIgnoreCase("info")) {
			GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
			if(!gp.hasParty()) {
				api.msg(p, ChatColor.RED, "참여 중인 파티가 없습니다!");
				return true;
			}
			Party party = api.getPartyManager().getParty(gp.getPartyName());
			api.msg(p, ChatColor.GREEN, "'" + party.getName() + "' 파티 정보");
			api.msg(p, ChatColor.GREEN, "인원 수: " + party.getPlayers().size());
			api.msg(p, ChatColor.GREEN, "참여 중인 플레이어: " + api.buildPlayerList(party.getPlayers()));
			return true;
		}
		return false;
	}
	
	public void sendHelpMessage(Player p) {
		api.msg(p, ChatColor.DARK_GREEN, ChatColor.GREEN + "[ U-HelpAbility - Party ]");
		api.msg(p, ChatColor.DARK_GREEN, ChatColor.GREEN + "제작자: " + ChatColor.WHITE + "ShaDow_Uni (셰도우 우니)");
		api.msg(p, ChatColor.DARK_GREEN, ChatColor.GREEN + "버전: " + ChatColor.WHITE + HelpAbility.getVersion());
		api.msg(p, ChatColor.DARK_GREEN, "/ua 파티 생성 <파티 이름>" + ChatColor.GREEN + " - 파티를 생성합니다.");
		api.msg(p, ChatColor.DARK_GREEN, "/ua 파티 초대 <플레이어>" + ChatColor.GREEN + " - 플레이어를 파티에 초대합니다.");
		api.msg(p, ChatColor.DARK_GREEN, "/ua 파티 수락" + ChatColor.GREEN + " - 파티 초대를 수락합니다.");
		api.msg(p, ChatColor.DARK_GREEN, "/ua 파티 거절" + ChatColor.GREEN + " - 파티 초대를 거절합니다.");
		api.msg(p, ChatColor.DARK_GREEN, "/ua 파티 탈퇴" + ChatColor.GREEN + " - 파티에서 탈퇴합니다.");
		api.msg(p, ChatColor.DARK_GREEN, "/ua 파티 채팅" + ChatColor.GREEN + " - 파티 채팅 모드로 전환하거나 해제합니다.");
	}
	
}
