package shadowuni.plugin.helpability.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;

public class KitCommand implements CommandInterface {
	
	private HelpAbilityAPI api = HelpAbility.getApi();

	@Override
	public boolean onCommand(Player p, String[] args) {
		if(!(args[0].equals("킷") || args[0].equalsIgnoreCase("kit"))) return false;
		if(!api.isAdmin(p, true)) return true;
		if(args.length < 2) {
			sendHelpMessage(p);
			return true;
		} else if(args[1].equals("생성") || args[0].equalsIgnoreCase("create")) {
			if(args.length < 3) {
				api.msg(p, ChatColor.BLUE, "사용법: /ha 킷 생성 <이름>");
				return true;
			} else if(api.getKitManager().existKit(args[2])) {
				api.msg(p, ChatColor.RED, "이미 존재하는 킷입니다!");
				return true;
			}
			api.getKitManager().createKit(args[2]);
			api.msg(p, ChatColor.BLUE, "'" + args[2] + "' 킷을 생성했습니다!");
			return true;
		} else if(args[1].equals("수정") || args[0].equalsIgnoreCase("edit")) {
			if(args.length < 3) {
				api.msg(p, ChatColor.BLUE, "사용법: /ha 킷 수정 <이름>");
				return true;
			} else if(!api.getKitManager().existKit(args[2])) {
				api.msg(p, ChatColor.RED, "존재하지 않는 킷입니다!");
				return true;
			}
			p.openInventory(api.getKitManager().getKit(args[2]));
			return true;
		} else if(args[1].equals("목록") || args[0].equalsIgnoreCase("list")) {
			if(api.getKitManager().getKits().size() < 1) {
				api.msg(p, ChatColor.RED, "아직 생성된 킷이 없습니다!");
				return true;
			}
			api.msg(p, ChatColor.YELLOW, api.buildStringList(api.getKitManager().getKits().keySet()));
			return true;
		}
		return false;
	}
	
	public void sendHelpMessage(Player p) {
		api.msg(p, ChatColor.BLUE, ChatColor.DARK_AQUA + "U-HelpAbility - Kit");
		api.msg(p, ChatColor.DARK_GREEN, ChatColor.DARK_AQUA + "제작자: " + ChatColor.WHITE + "ShaDow_Uni (셰도우 우니)");
		api.msg(p, ChatColor.DARK_GREEN, ChatColor.DARK_AQUA + "버전: " + ChatColor.WHITE + HelpAbility.getVersion());
		api.msg(p, ChatColor.BLUE, "/ha 킷 생성 <킷 이름>" + ChatColor.DARK_AQUA + " - 아이템 킷을 생성합니다.");
		api.msg(p, ChatColor.BLUE, "/ha 킷 수정 <킷 이름>" + ChatColor.DARK_AQUA + " - 아이템 킷을 수정합니다.");
		api.msg(p, ChatColor.BLUE, "/ha 킷 목록" + ChatColor.DARK_AQUA + " - 킷 목록을 확인합니다.");
	}

}
