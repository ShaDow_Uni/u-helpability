package shadowuni.plugin.helpability.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;

public class RankItemCommand implements CommandInterface {
	
	private HelpAbilityAPI api = HelpAbility.getApi();

	@Override
	public boolean onCommand(Player p, String[] args) {
		if(!(args[0].equals("등급아이템") || args[0].equalsIgnoreCase("rankItem"))) return false;
		if(!api.isAdmin(p, true)) return true;
		if(args.length < 2) {
			sendHelpMessage(p);
			return true;
		} else if(args[1].equals("주기") || args[0].equalsIgnoreCase("give")) {
			if(args.length < 3) {
				api.msg(p, ChatColor.BLUE, "사용법: /ha 등급아이템 주기 <플레이어>");
				return true;
			}
			Player target = Bukkit.getPlayer(args[2]);
			if(target == null) {
				api.msg(p, ChatColor.RED, "접속 중이 아닌 플레이어입니다!");
				return true;
			} else if(!api.getRankItemManager().hasItemGroup(target)) {
				api.msg(p, ChatColor.RED, "플레이어 등급의 아이템이 존재하지 않습니다!");
				return true;
			}
			api.getRankItemManager().giveItem(target);
			api.msg(p, ChatColor.BLUE, "'" + target.getName() + "' 님께 등급 아이템을 지급했습니다!");
			api.msg(target, ChatColor.BLUE, "" + p.getName() + "' 님게서 등급 아이템을 지급했습니다!");
			return true;
		} else if(args[1].equals("모두주기") || args[0].equalsIgnoreCase("giveAll")) {
			api.getRankItemManager().giveItemAll();
			api.broadcast(ChatColor.BLUE, "'" + p.getName() + "' 님께서 모두에게 등급 아이템을 지급했습니다!");
			return true;
		} else if(args[1].equals("등급목록") || args[0].equalsIgnoreCase("rankList")) {
			api.msg(p, ChatColor.BLUE, "등급 목록: " + api.buildStringList(api.getRankItemManager().getRankItems().keySet()));
			return true;
		} else if(args[1].equals("아이템목록") || args[0].equalsIgnoreCase("itemList")) {
			if(args.length < 3) {
				api.msg(p, ChatColor.BLUE, "사용법: /ha 등급아이템 아이템목록 <등급>");
				return true;
			} else if(!api.getRankItemManager().existRank(args[2])) {
				api.msg(p, ChatColor.RED, "존재하지 않는 등급입니다!");
				return true;
			}
			api.msg(p, ChatColor.BLUE, "'" + args[2] + "' 등급의 아이템 목록: " + api.buildItemListString(api.getRankItemManager().getItemList(args[2])));
			return true;
		}
		return false;
	}
	
	public void sendHelpMessage(Player p) {
		api.msg(p, ChatColor.BLUE, ChatColor.DARK_AQUA + "U-HelpAbility - RankItem");
		api.msg(p, ChatColor.DARK_GREEN, ChatColor.DARK_AQUA + "제작자: " + ChatColor.WHITE + "ShaDow_Uni (셰도우 우니)");
		api.msg(p, ChatColor.DARK_GREEN, ChatColor.DARK_AQUA + "버전: " + ChatColor.WHITE + HelpAbility.getVersion());
		api.msg(p, ChatColor.BLUE, "/ha 등급아이템 주기 <플레이어>" + ChatColor.DARK_AQUA + " - 플레이어에게 등급 아이템을 지급합니다.");
		api.msg(p, ChatColor.BLUE, "/ha 등급아이템 모두주기" + ChatColor.DARK_AQUA + " - 모든 플레이어에게 등급 아이템을 지급합니다.");
		api.msg(p, ChatColor.BLUE, "/ha 등급아이템 등급목록" + ChatColor.DARK_AQUA + " - 등급 목록을 확인합니다.");
		api.msg(p, ChatColor.BLUE, "/ha 등급아이템 아이템목록" + ChatColor.DARK_AQUA + " - 아이템 목록을 확인합니다.");
	}
	
}
