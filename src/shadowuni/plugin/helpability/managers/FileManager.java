package shadowuni.plugin.helpability.managers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Biome;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.objects.GameMap;

public class FileManager {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@Getter
	private File spawnFile = new File("plugins/U-HelpAbility/spawn.yml");
	@Getter
	private File mapFile = new File("plugins/U-HelpAbility/map.yml");
	@Getter
	private File rankFile = new File("plugins/U-HelpAbility/rankitem.yml");
	@Getter
	private File supplyFile = new File("plugins/U-HelpAbility/supply.yml");
	@Getter
	private File kitFolder = new File("plugins/U-HelpAbility/kits");
	@Getter
	private FileConfiguration spawnConfig = YamlConfiguration.loadConfiguration(spawnFile);
	@Getter
	private FileConfiguration mapConfig = YamlConfiguration.loadConfiguration(mapFile);
	@Getter
	private FileConfiguration rankConfig = YamlConfiguration.loadConfiguration(rankFile);
	@Getter
	private FileConfiguration supplyConfig = YamlConfiguration.loadConfiguration(supplyFile);
	
	public void createspawnFile() {
		if(!getSpawnFile().exists()) {
			try {
				getSpawnFile().createNewFile();
				api.log("스폰 파일이 생성되었습니다.");
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void createmapFile() {
		if(!getMapFile().exists()) {
			try {
				getMapFile().createNewFile();
				api.log("맵 파일이 생성되었습니다.");
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void createrankFile() {
		if(!getRankFile().exists()) {
			try {
				FileConfiguration f = YamlConfiguration.loadConfiguration(HelpAbility.getInstance().getResource("rankitem.yml"));
				f.save(rankFile);
				api.log("등급 아이템 설정 파일이 생성되었습니다.");
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void createsupplyFile() {
		if(!getSupplyFile().exists()) {
			try {
				FileConfiguration f = YamlConfiguration.loadConfiguration(HelpAbility.getInstance().getResource("supply.yml"));
				f.save(supplyFile);
				api.log("보급품 설정 파일이 생성되었습니다.");
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void createKitFolder() {
		if(!getKitFolder().exists()) {
			try {
				getKitFolder().mkdirs();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void saveSpawn() {
		GameMap map = api.getMapManager().getSpawnMap();
		getSpawnConfig().set("world", map.getMapLocation().getWorld().getName());
		getSpawnConfig().set("x", map.getMapLocation().getX());
		getSpawnConfig().set("y", map.getMapLocation().getY());
		getSpawnConfig().set("z", map.getMapLocation().getZ());
		getSpawnConfig().set("yaw", map.getMapLocation().getYaw());
		getSpawnConfig().set("pitch", map.getMapLocation().getPitch());
		try {
			getSpawnConfig().save(getSpawnFile());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveMap(GameMap map) {
		createmapFile();
		String loc = "맵." + map.getName().replace(".", "@") + ".";
		getMapConfig().set(loc + "랜덤 텔레포트", map.isRandomTeleport());
		getMapConfig().set(loc + "world", map.getMapLocation().getWorld().getName());
		getMapConfig().set(loc + "x", map.getMapLocation().getX());
		getMapConfig().set(loc + "y", map.getMapLocation().getY());
		getMapConfig().set(loc + "z", map.getMapLocation().getZ());
		getMapConfig().set(loc + "yaw", map.getMapLocation().getYaw());
		getMapConfig().set(loc + "pitch", map.getMapLocation().getPitch());
		if(map.getMinMapLocation() != null) {
			getMapConfig().set(loc + ".min.world", map.getMinMapLocation().getWorld().getName());
			getMapConfig().set(loc + ".min.x", map.getMinMapLocation().getX());
			getMapConfig().set(loc + ".min.y", map.getMinMapLocation().getY());
			getMapConfig().set(loc + ".min.z", map.getMinMapLocation().getZ());
		}
		if(map.getMaxMapLocation() != null) {
			getMapConfig().set(loc + ".max.world", map.getMaxMapLocation().getWorld().getName());
			getMapConfig().set(loc + ".max.x", map.getMaxMapLocation().getX());
			getMapConfig().set(loc + ".max.y", map.getMaxMapLocation().getY());
			getMapConfig().set(loc + ".max.z", map.getMaxMapLocation().getZ());
		}
		if(map.getTPAllLocation() != null) {
			getMapConfig().set(loc + ".tpall.world", map.getTPAllLocation().getWorld().getName());
			getMapConfig().set(loc + ".tpall.x", map.getTPAllLocation().getX());
			getMapConfig().set(loc + ".tpall.y", map.getTPAllLocation().getY());
			getMapConfig().set(loc + ".tpall.z", map.getTPAllLocation().getZ());
			getMapConfig().set(loc + ".tpall.yaw", map.getTPAllLocation().getYaw());
			getMapConfig().set(loc + ".tpall.pitch", map.getTPAllLocation().getPitch());
		}
		try {
			getMapConfig().save(getMapFile());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveKit(Inventory inv) {
		try {
			File kitfile = new File("plugins/U-HelpAbility/kit/" + inv.getName() + ".yml");
			if(kitfile.exists()) {
				kitfile.delete();
			}
			kitfile.createNewFile();
			FileConfiguration kitconfig = YamlConfiguration.loadConfiguration(kitfile);
			int i = 0;
			for(ItemStack item : inv) {
				kitconfig.set(i + "", item);
				i++;
			}
			kitconfig.save(kitfile);
			api.log(inv.getName() + " 킷을 저장했습니다!");
		} catch(Exception e) {
			api.log(inv.getName() + " 킷을 저장할 수 없습니다!");
			e.printStackTrace();
		}
	}
	
	public void deleteMap(GameMap map) {
		createmapFile();
		String loc = "맵." + map.getName().replace(".", "@");
		if(getMapConfig().getString(loc + ".world") == null) return;
		getMapConfig().set(loc, null);
		try {
			getMapConfig().save(getMapFile());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadSpawn() {
		if(!getSpawnFile().exists()) return;
		if(getSpawnFile().length() < 1) return;
		World world = Bukkit.getWorld(getSpawnConfig().getString("world"));
		if(world == null) {
			world = Bukkit.createWorld(new WorldCreator(getSpawnConfig().getString("world")));
		}
		double x = getSpawnConfig().getDouble("x");
		double y = getSpawnConfig().getDouble("y");
		double z = getSpawnConfig().getDouble("z");
		float yaw = (float) getSpawnConfig().getDouble("yaw");
		float pitch = (float) getSpawnConfig().getDouble("pitch");
		Location l = new Location(world, x, y, z, yaw, pitch);
		api.getMapManager().setSpawnLocation(l);
		api.log("스폰을 불러왔습니다.");
	}
	
	public void loadMap(String map) {
		if(!getMapFile().exists()) return;
		if(getMapFile().length() < 1) return;
		String loc = "맵." + map + ".";
		World world = Bukkit.getWorld(getMapConfig().getString(loc + "world"));
		if(world == null) {
			world = Bukkit.createWorld(new WorldCreator(getMapConfig().getString(loc + "world")));
		}
		GameMap m = new GameMap();
		m.setName(map.replace("@", "."));
		m.setRandomTeleport(mapConfig.getBoolean(loc + "랜덤 텔레포트"));
		double x;
		double y;
		double z;
		float yaw;
		float pitch;
		Location l;
		if(getMapConfig().getString(loc + "x").equals("<월드스폰>")) {
			world.getSpawnLocation().getChunk().load();
			world.getSpawnLocation().getChunk().unload();
			l = world.getHighestBlockAt((int) world.getSpawnLocation().getX(), (int) world.getSpawnLocation().getZ()).getLocation();
			if(l.getY() < 40 || world.getBlockAt(l).equals(Biome.OCEAN)) {
				Bukkit.shutdown();
				api.log("플레이가 불가능한 월드가 생성되어 서버가 종료됩니다.");
				return;
			}
			l.setY(l.getY() + 1);
			m.setRandomTeleport(false);
			int j = (int) Math.pow(api.getAutoMapLimitRange() + 1, 2);
			for(int i = 0; i < j; i++) {
				double px = l.getX() + Math.cos(i) * api.getAutoMapLimitRange();
				double pz = l.getZ() + Math.sin(i) * api.getAutoMapLimitRange();
				Location location = world.getHighestBlockAt((int) px, (int) pz).getLocation();
				location.setY(location.getY() + 3);
				m.addPacketLocation(location);
			}
		} else {
			x = getMapConfig().getDouble(loc + "x");
			y = getMapConfig().getDouble(loc + "y");
			z = getMapConfig().getDouble(loc + "z");
			yaw = (float) getMapConfig().getDouble(loc + "yaw");
			pitch = (float) getMapConfig().getDouble(loc + "pitch");
			l = new Location(world, x, y+1, z, yaw, pitch);
		}
		m.setMapLocation(l);
		if(getMapConfig().getString(loc + "min.world") != null && getMapConfig().getString(loc + "max.world") != null) {
			World world1 = Bukkit.getWorld(getMapConfig().getString(loc + "min.world"));
			double x1 = getMapConfig().getDouble(loc + "min.x");
			double y1 = getMapConfig().getDouble(loc + "min.y");
			double z1 = getMapConfig().getDouble(loc + "min.z");
			Location loc1 = new Location(world1, x1, y1, z1);
			World world2 = Bukkit.getWorld(getMapConfig().getString(loc + "max.world"));
			double x2 = getMapConfig().getDouble(loc + "max.x");
			double y2 = getMapConfig().getDouble(loc + "max.y");
			double z2 = getMapConfig().getDouble(loc + "max.z");
			Location loc2 = new Location(world2, x2, y2, z2);
			m.setMapLimitLocation(loc1, loc2);
		}
		if(getMapConfig().getString(loc + "tpall.world") != null) {
			World tworld = Bukkit.getWorld(getMapConfig().getString(loc + "tpall.world"));
			double tx;
			double ty;
			double tz;
			float tyaw;
			float tpitch;
			Location tl = null;
			if(getMapConfig().getString(loc + "tpall.x").equals("<월드스폰>")) {
				tl = tworld.getHighestBlockAt((int) tworld.getSpawnLocation().getX(), (int) tworld.getSpawnLocation().getZ()).getLocation();
			} else {
				tworld = Bukkit.getWorld(mapConfig.getString(loc + "tpall.world"));
				tx = getMapConfig().getDouble(loc + "tpall.x");
				ty = getMapConfig().getDouble(loc + "tpall.y");
				tz = getMapConfig().getDouble(loc + "tpall.z");
				tyaw = (float) getMapConfig().getDouble(loc + "tpall.yaw");
				tpitch = (float) getMapConfig().getDouble(loc + "tpall.pitch");
				tl = new Location(tworld, tx, ty, tz, tyaw, tpitch);
			}
			m.setTPAllLocation(tl);
		}
		api.getMapManager().setMap(map, m);
	}
	
	public void loadAllMap() {
		if(!getMapFile().exists()) return;
		if(getMapFile().length() < 1) return;
		int i = 0;
		for(String map : getMapConfig().getConfigurationSection("맵").getKeys(false)) {
			loadMap(map);
			i++;
		}
		api.log(i + "개의 맵을 불러왔습니다!");
	}
	
	public void loadKit() {
		try {
			File kits = new File("plugins/U-HelpAbility/kit");
			if(!kits.exists()) {
				kits.mkdir();
				api.log("킷 폴더를 생성했습니다!");
				return;
			}
			if(kits.listFiles().length < 1) return;
			int load = 0;
			for(File kit : kits.listFiles()) {
				FileConfiguration kitconfig = YamlConfiguration.loadConfiguration(kit);
				String invname = kit.getName().substring(0, kit.getName().length() - 4);
				Inventory inv = Bukkit.createInventory(null, 36, invname);
				for(int i = 0; i < 36; i++) {
					if(kitconfig.get(i + "") != null) {
						inv.setItem(i, kitconfig.getItemStack(i + ""));
					}
				}
				api.getKitManager().setKit(invname, inv);
				load++;
			}
			api.log(load + "개의 킷을 불러왔습니다!");
		} catch(Exception e) {
			api.log("킷을 불러올 수 없습니다!");
		}
	}
	
	public void loadRankItem() {
		if(getRankFile().length() < 1) {
			createrankFile();
			return;
		} else if(api.getRankItemManager().getRankItems().size() > 0) {
			api.getRankItemManager().getRankItems().clear();
		}
		int i = 0;
		for(String rank : getRankConfig().getConfigurationSection("등급").getKeys(false)) {
			List<String> itemliststr = getRankConfig().getStringList("등급." + rank);
			ArrayList<ItemStack> itemlista = new ArrayList<>();
			for(String items : itemliststr) {
				if(items.startsWith("<kit:")) {
					String kitname = items.substring(5, items.length() - 1);
					if(!api.getKitManager().existKit(kitname)) {
						api.log("'" + kitname + "' 킷을 찾을 수 없습니다!");
						return;
					}
					for(ItemStack item : api.getKitManager().getKit(kitname)) {
						if(item != null) {
							itemlista.add(item);
						}
					}
				} else {
					String[] item = items.split(" ");
					if(item[0].contains(":")) {
						String[] info = item[0].split(":");
						itemlista.add(new ItemStack(Material.getMaterial(Integer.valueOf(info[0])), Integer.valueOf(item[1]), Short.valueOf(info[1])));
					} else {
						itemlista.add(new ItemStack(Integer.valueOf(item[0]), Integer.valueOf(item[1])));
					}
				}
			}
			api.getRankItemManager().setRank(rank, itemlista);
			i++;
		}
		api.log(i + "개 등급의 아이템을 등록했습니다.");
	}
	
	public void loadSupply() {
		if(getSupplyFile().length() < 1) {
			createsupplyFile();
			return;
		} else if(api.getSupplyManager().getSupplies().size() > 0) {
			api.getSupplyManager().getSupplies().clear();
		}
		int i = 0;
		for(String name : getSupplyConfig().getConfigurationSection("보급품").getKeys(false)) {
			List<String> itemliststr = getSupplyConfig().getStringList("보급품." + name);
			ArrayList<ItemStack> itemlista = new ArrayList<>();
			for(String items : itemliststr) {
				if(items.startsWith("<kit:")) {
					String kitname = items.substring(5, items.length() - 1);
					if(!api.getKitManager().existKit(kitname)) {
						api.log("'" + kitname + "' 킷을 찾을 수 없습니다!");
						return;
					}
					for(ItemStack item : api.getKitManager().getKit(kitname)) {
						if(item != null) {
							itemlista.add(item);
						}
					}
				} else {
					String[] item = items.split(" ");
					if(item[0].contains(":")) {
						String[] info = item[0].split(":");
						itemlista.add(new ItemStack(Material.getMaterial(Integer.valueOf(info[0])), Integer.valueOf(item[1]), Short.valueOf(info[1])));
					} else {
						itemlista.add(new ItemStack(Integer.valueOf(item[0]), Integer.valueOf(item[1])));
					}
				}
			}
			api.getSupplyManager().setSupply(name, itemlista);
			i++;
		}
		api.log(i + "개 등급의 보급품을 등록했습니다.");
	}
	
}
