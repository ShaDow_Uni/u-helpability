package shadowuni.plugin.helpability.managers;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;

public class ItemManager {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@Getter
	private HashMap<String, ArrayList<ItemStack>> rankItems = new HashMap<>();
	@Getter
	private HashMap<String, Inventory> kits = new HashMap<>();
	
	public void setRank(String rank, ArrayList<ItemStack> items) {
		rankItems.put(rank.toLowerCase(), items);
	}
	
	public void setKit(String kit, Inventory inventory) {
		kits.put(kit.toLowerCase(), inventory);
	}
	
	public Inventory createKit(String name) {
		Inventory inv = Bukkit.createInventory(null, 27, name);
		setKit(name, inv);
		return inv;
	}
	
	public boolean existRank(String rank) {
		return rankItems.containsKey(rank.toLowerCase());
	}
	
	public boolean existKit(String kit) {
		return kits.containsKey(kit.toLowerCase());
	}
	
	public ArrayList<ItemStack> getItemList(String rank) {
		if(!existRank(rank)) return null;
		return rankItems.get(rank.toLowerCase());
	}
	
	public Inventory getKit(String kit) {
		if(!existKit(kit)) return null;
		return kits.get(kit.toLowerCase());
	}
	
	public boolean hasItemGroup(Player p) {
		String[] group = api.getLibrary().getVault().getChat().getPlayerGroups(p);
		for(String name : group) {
			if(existRank(name)) {
				return true;
			}
		}
		return false;
	}
	
	public void giveItem(Player p) {
		if(!hasItemGroup(p)) return;
		String[] group = api.getLibrary().getVault().getChat().getPlayerGroups(p);
		for(int i = 0; i < group.length; i++) {
			for(ItemStack item : getItemList(group[i])) {
				p.getInventory().addItem(item);
			}
		}
	}
	
	public void giveItemAll() {
		for(Player ap : Bukkit.getOnlinePlayers()) {
			giveItem(ap);
		}
	}
	
}