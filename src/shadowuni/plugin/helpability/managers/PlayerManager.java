package shadowuni.plugin.helpability.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;

import lombok.Getter;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class PlayerManager {
	
	@Getter
	private HashMap<String, GamePlayer> players = new HashMap<>();
	
	public void setGamePlayer(Player p, GamePlayer gp) {
		setGamePlayer(p.getName(), gp);
	}
	
	public void setGamePlayer(String name, GamePlayer gp) {
		players.put(name.toLowerCase(), gp);
	}
	
	public boolean existGamePlayer(Player p) {
		return existGamePlayer(p.getName());
	}
	
	public boolean existGamePlayer(String name) {
		return players.containsKey(name.toLowerCase());
	}
	
	public GamePlayer getGamePlayer(Player p) {
		return getGamePlayer(p.getName());
	}
	
	public GamePlayer getGamePlayer(String name) {
		if(!existGamePlayer(name)) return null;
		return players.get(name.toLowerCase());
	}
	
	public boolean existPlayer(Player p) {
		return existPlayer(p.getName());
	}
	
	public boolean existPlayer(String name) {
		return players.containsKey(name.toLowerCase());
	}
	
	public List<GamePlayer> getAllPlayers() {
		List<GamePlayer> playerlist = new ArrayList<>();
		for(GamePlayer gp : players.values()) {
			playerlist.add(gp);
		}
		return playerlist;
	}
	
	public List<GamePlayer> getGamePlayers() {
		List<GamePlayer> list = new ArrayList<>();
		for(GamePlayer gp : players.values()) {
			if(!gp.isOnline() || gp.isDead() || gp.isWatchMode()) continue;
			list.add(gp);
		}
		return list;
	}
	
	public List<GamePlayer> getAllGamePlayers() {
		List<GamePlayer> list = new ArrayList<>();
		for(GamePlayer gp : players.values()) {
			if(gp.isDead() || gp.isWatchMode()) continue;
			list.add(gp);
		}
		return list;
	}
	
	public List<GamePlayer> getDeadPlayers() {
		List<GamePlayer> list = new ArrayList<>();
		for(GamePlayer gp : players.values()) {
			if(!gp.isDead()) continue;
			list.add(gp);
		}
		return list;
	}
	
	public List<GamePlayer> getWatchPlayers() {
		List<GamePlayer> list = new ArrayList<>();
		for(GamePlayer gp : players.values()) {
			if(!gp.isOnline() || !gp.isWatchMode()) continue;
			list.add(gp);
		}
		return list;
	}
	
	public List<GamePlayer> getAllWatchPlayers() {
		List<GamePlayer> list = new ArrayList<>();
		for(GamePlayer gp : players.values()) {
			if(!gp.isWatchMode()) continue;
			list.add(gp);
		}
		return list;
	}
	
}
