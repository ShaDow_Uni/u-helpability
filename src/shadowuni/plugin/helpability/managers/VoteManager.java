package shadowuni.plugin.helpability.managers;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;

public class VoteManager {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	@Setter
	@Getter
	private boolean voting = false;
	@Getter
	private List<String> agree = new ArrayList<>();
	@Getter
	private List<String> disagree = new ArrayList<>();
	@Setter
	@Getter
	private int voteTask;
	
	public void init() {
		voting = false;
		agree.clear();
		disagree.clear();
	}
	
	public void addAgree(Player p) {
		addAgree(p.getName());
	}
	
	public void addAgree(String name) {
		agree.add(name.toLowerCase());
	}
	
	public void addDisAgree(Player p) {
		addDisAgree(p.getName());
	}
	
	public void addDisAgree(String name) {
		disagree.add(name.toLowerCase());
	}
	
	public void joinVote(Player p, boolean agree) {
		joinVote(p.getName(), agree);
	}
	
	public void joinVote(String name, boolean agree) {
		if(agree) {
			addAgree(name);
		} else {
			addDisAgree(name);
		}
		api.broadcast(ChatColor.DARK_AQUA, this.agree.size() + disagree.size() + "명이 투표에 참여했습니다! (찬성: " + this.agree.size() + " / 반대: " + disagree.size() + ")");
		int playercount = api.getPlayerManager().getGamePlayers().size();
		if(playercount % 2 == 0 && this.agree.size() >= playercount / 2) {
			stopVote();
			api.getGameManager().GameStart(null);
		} else if(playercount % 2 != 0 && this.agree.size()  >= (playercount + 1) / 2) {
			stopVote();
			api.getGameManager().GameStart(null);
		} else if(playercount % 2 == 0 && this.disagree.size() >= playercount / 2) {
			stopVote();
			api.broadcast(ChatColor.YELLOW, "투표가 부결되었습니다!");
		} else if(playercount % 2 != 0 && this.disagree.size()  >= (playercount + 1) / 2) {
			stopVote();
			api.broadcast(ChatColor.YELLOW, "투표가 부결되었습니다!");
		}
	}
	
	public boolean isAgree(Player p) {
		return isAgree(p.getName());
	}
	
	public boolean isAgree(String name) {
		return agree.contains(name.toLowerCase());
	}
	
	public boolean isDisAgree(Player p) {
		return isDisAgree(p.getName());
	}
	
	public boolean isDisAgree(String name) {
		return disagree.contains(name.toLowerCase());
	}
	
	public boolean isVoted(Player p) {
		return isVoted(p.getName());
	}
	
	public boolean isVoted(String name) {
		return isAgree(name) || isDisAgree(name);
	}
	
	public void startVote(int time) {
		init();
		voting = true;
		api.getTaskManager().runVoteTask(time);
	}
	
	public void stopVote() {
		init();
		api.getTaskManager().stopVoteTask();
	}

}
