package shadowuni.plugin.helpability.managers;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.events.RankItemGiveEvent;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class RankItemManager {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@Getter
	private HashMap<String, ArrayList<ItemStack>> rankItems = new HashMap<>();
	
	public void setRank(String rank, ArrayList<ItemStack> items) {
		rankItems.put(rank.toLowerCase(), items);
	}
	
	public boolean existRank(String rank) {
		return rankItems.containsKey(rank.toLowerCase());
	}
	
	public ArrayList<ItemStack> getItemList(String rank) {
		if(!existRank(rank)) return null;
		return rankItems.get(rank.toLowerCase());
	}
	
	public boolean hasItemGroup(Player p) {
		String[] group = api.getLibrary().getVault().getChat().getPlayerGroups(p);
		for(String name : group) {
			if(existRank(name)) {
				return true;
			}
		}
		return false;
	}
	
	public void giveItem(Player p) {
		if(!hasItemGroup(p)) return;
		String[] group = api.getLibrary().getVault().getChat().getPlayerGroups(p);
		for(int i = 0; i < group.length; i++) {
			for(ItemStack item : getItemList(group[i])) {
				p.getInventory().addItem(item);
			}
		}
	}
	
	public void giveItemAll() {
		for(GamePlayer gp : api.getPlayerManager().getGamePlayers()) {
			RankItemGiveEvent event = new RankItemGiveEvent(gp.getPlayer());
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled()) continue;
			giveItem(gp.getPlayer());
		}
	}
	
}
