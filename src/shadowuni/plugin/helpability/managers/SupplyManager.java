package shadowuni.plugin.helpability.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.objects.GameMap;

public class SupplyManager {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@Getter
	private HashMap<String, ArrayList<ItemStack>> supplies = new HashMap<>();
	
	public void setSupply(String name, ArrayList<ItemStack> items) {
		supplies.put(name.toLowerCase(), items);
	}
	
	public boolean existSupply(String name) {
		return supplies.containsKey(name.toLowerCase());
	}
	
	public ArrayList<ItemStack> getSupply(String name) {
		return supplies.get(name.toLowerCase());
	}
	
	public ArrayList<ItemStack> getRandomSupply() {
		if(supplies.size() < 1) return null;
		String[] supplystr = new String[supplies.size()];
		int i = 0;
		for(String sname : supplies.keySet()) {
			supplystr[i] = sname;
			i++;
		}
		int randomcount = new Random().nextInt(supplystr.length);
		return getSupply(supplystr[randomcount]);
	}
	
	public boolean createSupply(Location location, final ArrayList<ItemStack> supply) {
		final Block b = location.getBlock();
		b.setType(Material.CHEST);
		Bukkit.getScheduler().scheduleSyncDelayedTask(HelpAbility.getInstance(), new Runnable() {
			public void run() {
				if(api.isUseSupplyFirework()) {
					api.spawnFirework(location);
				}
				Chest c = (Chest) b.getState();
				int i = 0;
				for(ItemStack item : supply) {
					if(i <36) {
						c.getInventory().addItem(item);
						i++;
					}
				}
				
			}
		}, 2);
		return true;
	}
	
	public boolean createRandomSupply(Location location) {
		if(supplies.size() < 1) return false;
		return createSupply(location, getRandomSupply());
	}
	
	public Location createSupplyAtRandomLocation(GameMap map, ArrayList<ItemStack> supply) {
		Location location = map.getRandomLocation();
		createSupply(location, supply);
		return location;
	}
	
	public Location createRandomSupplyAtRandomLocation(GameMap map) {
		if(supplies.size() < 1) return null;
		return createSupplyAtRandomLocation(map, getRandomSupply());
	}
	
}
