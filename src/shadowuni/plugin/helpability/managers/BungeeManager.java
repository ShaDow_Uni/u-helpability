package shadowuni.plugin.helpability.managers;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;

public class BungeeManager {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	public void sendTo(Player p, String server) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Connect");
		out.writeUTF(server);
		p.sendPluginMessage(HelpAbility.getInstance(), "BungeeCord", out.toByteArray());
	}
	
	public void sendMessage(String to, String msg) {
		if(msg.contains("\n")) {
			String[] s = msg.split("\n");
			for(int i = 0; i < s.length; i++) {
				sendMessage(to, s[i]);
			}
		}
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Message");
		out.writeUTF(to);
		out.writeUTF(msg);
		Bukkit.getOnlinePlayers()[0].sendPluginMessage(HelpAbility.getInstance(), "BungeeCord", out.toByteArray());
	}
	
	@SuppressWarnings("deprecation")
	public void broadcast(final String msg) {
		Bukkit.getScheduler().scheduleAsyncDelayedTask(HelpAbility.getInstance(), new Runnable() {
			public void run() {
				for(String player : api.getBungeePlayerList()) {
					sendMessage(player, msg);
				}
			}
		}, 10);
	}
	
	public void getPlayerList(String server) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("PlayerList");
		out.writeUTF(server);
		Bukkit.getOnlinePlayers()[0].sendPluginMessage(HelpAbility.getInstance(), "BungeeCord", out.toByteArray());
	}
	
	public void getServerName() {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("GetServer");
		Bukkit.getOnlinePlayers()[0].sendPluginMessage(HelpAbility.getInstance(), "BungeeCord", out.toByteArray());
	}

}
