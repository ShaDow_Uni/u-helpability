package shadowuni.plugin.helpability.managers;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import lombok.Getter;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.objects.GamePlayer;
import shadowuni.plugin.helpability.objects.Party;

public class PartyManager {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@Getter
	private HashMap<String, Party> partys = new HashMap<>();
	
	public void setParty(String name, Party party) {
		partys.put(name, party);
	}
	
	public Party createParty(String name) {
		Party party = new Party();
		party.setName(name);
		setParty(name, party);
		return party;
	}
	
	public void removeParty(String name) {
		if(!existParty(name)) return;
		partys.remove(name);
	}
	
	public Party getParty(String name) {
		if(!existParty(name)) return null;
		return partys.get(name);
	}
	
	public boolean existParty(String name) {
		return partys.containsKey(name);
	}
	
	public void sendPartyChat(Player p, String msg) {
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(!gp.hasParty()) return;
		Party party = getParty(gp.getPartyName());
		String chat = ChatColor.WHITE + "파티 채팅 :: " + api.replaceColor(api.getLibrary().getVault().getChat().getPlayerPrefix(p)) + ChatColor.WHITE + gp.getDisplayName() + " > ";
		if(ChatColor.getLastColors(chat).equals("§f")) {
			chat += ChatColor.GREEN + msg;
		} else {
			chat += msg;
		}
		for(Player pp : party.getPlayers()) {
			if(pp == null) continue;
			api.nmsg(pp, ChatColor.GREEN + "◆ " + chat);
		}
		api.log("'" + party.getName() + "' " + chat);
	}

}
