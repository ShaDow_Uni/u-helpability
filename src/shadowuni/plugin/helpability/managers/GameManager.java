package shadowuni.plugin.helpability.managers;

import java.lang.reflect.Field;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Physical.Fighters.PhysicalFighters;
import Xeon.VisualAbility.VisualAbility;
import Xeon.VisualAbility.MinerModule.TimerBase;
import Xeon.VisualAbility.Script.MainScripter;
import Xeon.VisualAbility.Script.S_GameStart;
import Xeon.VisualAbility.Script.S_GameStart.S_ScriptTimer;
import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.bungeeparty.objects.Party;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.category.GameCommand;
import shadowuni.plugin.helpability.category.GameState;
import shadowuni.plugin.helpability.events.GameStartEvent;
import shadowuni.plugin.helpability.events.GameStopEvent;
import shadowuni.plugin.helpability.events.WinEvent;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class GameManager {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@Setter
	@Getter
	private GameState gameState = GameState.WAITING;
	
	@Setter
	@Getter
	private boolean teleportedAll = false;
	
	@Setter
	@Getter
	private int TPAllCount;
	
	@Setter
	@Getter
	private long startTime;
	
	public boolean isGameStarted() {
		return !getGameState().equals(GameState.WAITING);
	}
	
	@SuppressWarnings("static-access")
	public boolean isAbilitySelected() {
		if(api.isUseBitAbility()) {
			MainScripter s = ((VisualAbility) Bukkit.getPluginManager().getPlugin("BitAbility")).scripter;
			return s.OKSign.size() == s.PlayerList.size();
		} else if (api.isUseDyeAbility()) {
			Physical.Fighters.Script.MainScripter s = ((PhysicalFighters) Bukkit.getPluginManager().getPlugin("PhysicalFighters")).scripter;
			return s.OKSign.size() == s.PlayerList.size();
		}
		return false;
	}
	
	public boolean isAbilityGameStarted() {
		try {
			if(api.isUseBitAbility()) {
				MainScripter s = ((VisualAbility) Bukkit.getPluginManager().getPlugin("BitAbility")).scripter;
				Field field = S_GameStart.class.getDeclaredField("stimer");
				field.setAccessible(true);
				S_ScriptTimer timer = (S_ScriptTimer) field.get(s.s_GameStart);
				Field cfield = TimerBase.class.getDeclaredField("Count");
				cfield.setAccessible(true);
				int count = cfield.getInt(timer);
				return count >= 15;
			} else if (api.isUseDyeAbility()) {
				Physical.Fighters.Script.MainScripter s = ((PhysicalFighters) Bukkit.getPluginManager().getPlugin("PhysicalFighters")).scripter;
				Field field = Physical.Fighters.Script.S_GameStart.class.getDeclaredField("stimer");
				field.setAccessible(true);
				Physical.Fighters.Script.S_GameStart.S_ScriptTimer timer = (Physical.Fighters.Script.S_GameStart.S_ScriptTimer) field.get(s.s_GameStart);
				Field cfield = Physical.Fighters.MinerModule.TimerBase.class.getDeclaredField("Count");
				cfield.setAccessible(true);
				int count = cfield.getInt(timer);
				return count >= 15;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean isAbilityGameStarting() {
		try {
			if(api.isUseBitAbility()) {
				MainScripter s = ((VisualAbility) Bukkit.getPluginManager().getPlugin("BitAbility")).scripter;
				Field field = S_GameStart.class.getDeclaredField("stimer");
				field.setAccessible(true);
				S_ScriptTimer timer = (S_ScriptTimer) field.get(s.s_GameStart);
				Field cfield = TimerBase.class.getDeclaredField("Count");
				cfield.setAccessible(true);
				int count = cfield.getInt(timer);
				return count > 0;
			} else if (api.isUseDyeAbility()) {
				Physical.Fighters.Script.MainScripter s = ((PhysicalFighters) Bukkit.getPluginManager().getPlugin("PhysicalFighters")).scripter;
				Field field = Physical.Fighters.Script.S_GameStart.class.getDeclaredField("stimer");
				field.setAccessible(true);
				Physical.Fighters.Script.S_GameStart.S_ScriptTimer timer = (Physical.Fighters.Script.S_GameStart.S_ScriptTimer) field.get(s.s_GameStart);
				Field cfield = Physical.Fighters.MinerModule.TimerBase.class.getDeclaredField("Count");
				cfield.setAccessible(true);
				int count = cfield.getInt(timer);
				return count > 0;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public long getPlayTime() {
		return System.currentTimeMillis() - getStartTime();
	}
	
	public void GameStart(CommandSender starter) {
		if(isGameStarted()) return;
		GameStartEvent event = new GameStartEvent(starter);
		Bukkit.getPluginManager().callEvent(event);
		if(event.isCancelled()) return;
		setStartTime(System.currentTimeMillis());
		api.getVoteManager().stopVote();
		api.getTaskManager().runStartStateUpdateTask();
		api.getTaskManager().runProjectilePassTask();
		if(api.isUseSupply()) {
			api.getTaskManager().runSupplyTask();
		}
		if(api.isUseMapLimitParticle()) {
			api.getTaskManager().runLimitParticleTask();
		}
		setGameState(GameState.PREPARING);
		api.getTaskManager().runGameStartTask();
	}
	
	@SuppressWarnings("deprecation")
	public void GameStop(CommandSender stopper) {
		if(!isGameStarted()) return;
		Bukkit.getPluginManager().callEvent(new GameStopEvent(stopper));
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), GameCommand.STOP);
		api.getBarApi().clearBar();
		setGameState(GameState.WAITING);
		api.getTaskManager().initAllTask();
		api.getMapManager().setPlayingMap(null);
		setStartTime(0);
		setTeleportedAll(false);
		setTPAllCount(-1);
		for(GamePlayer agp : api.getPlayerManager().getAllPlayers()) {
			if(agp.getPlayer() == null) continue;
			if(api.getMapManager().getSpawnMap() != null) {
				if(agp.getPlayer().getVehicle() != null) {
					agp.getPlayer().leaveVehicle();
				}
				agp.getPlayer().getInventory().clear();
				agp.getPlayer().updateInventory();
				agp.getPlayer().teleport(api.getMapManager().getSpawnMap().getMapLocation());
			}
			if(agp.isWatchMode()) {
				agp.setWatchMode(false);
				agp.showPlayer();
				agp.getPlayer().setAllowFlight(false);
				agp.getPlayer().setFlying(false);
				agp.getPlayer().setGameMode(GameMode.SURVIVAL);
				api.msg(agp.getPlayer(), ChatColor.BLUE, "관전 모드가 해제되었습니다.");
			}
			agp.setDead(false);
			agp.setLastKillTime(0);
			agp.setLastKillType(null);
			agp.setQuitTime(0);
		}
	}
	
	public boolean canFinish() {
		if(api.getGameManager().getGameState().equals(GameState.END) || api.getGameManager().getPlayTime() < api.getWinMinCount() * 1000) return false;
		if(api.isUseBungeeParty() && api.getPlayerManager().getGamePlayers().size() > 1) {
			for(Party party : api.getPartyApi().getPartyManager().getPartys().values()) {
				int gpc = 0;
				for(Player pp : party.getOnlinePlayers()) {
					GamePlayer gp = api.getPlayerManager().getGamePlayer(pp);
					if(!gp.isOnline() || gp.isDead() || gp.isWatchMode()) continue;
					gpc++;
				}
				if(gpc == api.getPlayerManager().getGamePlayers().size()) return true;
			}
			return false;
		}
		return api.getPlayerManager().getGamePlayers().size() < 2;
	}
	
	public boolean finish() {
		if(!canFinish()) return false;
		List<GamePlayer> winners = api.getPlayerManager().getGamePlayers();
		if(winners.size() < 1) return false;
		WinEvent event = new WinEvent(winners, api.getWinMoney());
		Bukkit.getPluginManager().callEvent(event);
		if(event.isCancelled()) return false;
		api.getGameManager().setGameState(GameState.END);
		StringBuilder builder = new StringBuilder();
		for(GamePlayer gp : winners) {
			builder.append(builder.length() < 1 ? "'" + gp.getDisplayName() + "'" : ChatColor.WHITE + ", '" + gp.getDisplayName() + "'");
			api.getLibrary().getVault().giveMoney(gp.getPlayer(), Math.round(event.getWinMoney() / winners.size()));
			api.msg(gp.getPlayer(), ChatColor.DARK_AQUA, "게임에서 승리했습니다! 축하드립니다!");
			api.msg(gp.getPlayer(), ChatColor.DARK_AQUA, "보상: " + Math.round(event.getWinMoney() / winners.size()));
		}
		api.broadcast(ChatColor.DARK_GREEN, "'" + builder.toString() + "' 님께서 우승하셨습니다. 축하드립니다!");
		return true;
	}
	
}