package shadowuni.plugin.helpability.managers;

import java.util.HashMap;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.helpability.objects.GameMap;

public class MapManager {
	
	@Setter
	@Getter
	private GameMap spawnMap, playingMap;
	@Getter
	private HashMap<String, GameMap> maps = new HashMap<>();
	private HashMap<String, Location> leftLocation = new HashMap<>();
	private HashMap<String, Location> rightLocation = new HashMap<>();
	
	public void setSpawnLocation(Location location) {
		GameMap map = new GameMap();
		map.setMapLocation(location);
		setSpawnMap(map);
	}
	
	public void setMap(String name, GameMap map) {
		maps.put(name.toLowerCase(), map);
	}
	
	public void setLeftLocation(Player p, Location location) {
		leftLocation.put(p.getName().toLowerCase(), location);
	}
	
	public void setRightLocation(Player p, Location location) {
		rightLocation.put(p.getName().toLowerCase(), location);
	}
	
	public void deleteMap(GameMap map) {
		deleteMap(map.getName());
	}
	
	public void deleteMap(String name) {
		if(!existMap(name)) return;
		maps.remove(name.toLowerCase());
	}
	
	public boolean existMap(String name) {
		return maps.containsKey(name.toLowerCase());
	}
	
	public boolean existLeftLocation(Player p) {
		return leftLocation.containsKey(p.getName().toLowerCase());
	}
	
	public boolean existRightLocation(Player p) {
		return rightLocation.containsKey(p.getName().toLowerCase());
	}
	
	public GameMap getMap(String name) {
		if(!existMap(name)) return null;
		return maps.get(name.toLowerCase());
	}
	
	public Location getLeftLocation(Player p) {
		return leftLocation.get(p.getName().toLowerCase());
	}
	
	public Location getRightLocation(Player p){
		return rightLocation.get(p.getName().toLowerCase());
	}
	
	public GameMap getRandomMap() {
		if(maps.size() < 1) return null;
		String[] mapstr = new String[maps.size()];
		int i = 0;
		for(String mapname : maps.keySet()) {
			mapstr[i] = mapname;
			i++;
		}
		Random r = new Random();
		int randomcount = r.nextInt(mapstr.length);
		return getMap(mapstr[randomcount]);
	}
	
}
