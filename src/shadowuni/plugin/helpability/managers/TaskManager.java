package shadowuni.plugin.helpability.managers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.category.GameState;
import shadowuni.plugin.helpability.tasks.DrawSkipTask;
import shadowuni.plugin.helpability.tasks.GameStartTask;
import shadowuni.plugin.helpability.tasks.LimitParticleTask;
import shadowuni.plugin.helpability.tasks.LocationNotifyTask;
import shadowuni.plugin.helpability.tasks.ProjectilePassTask;
import shadowuni.plugin.helpability.tasks.StartStateUpdateTask;
import shadowuni.plugin.helpability.tasks.SupplyTask;
import shadowuni.plugin.helpability.tasks.TPAllTask;
import shadowuni.plugin.helpability.tasks.VoteTask;

@Setter
@Getter
public class TaskManager {
	
	HelpAbilityAPI api = HelpAbility.getApi();
	
	private int gamerunTaskId, drawSkipTaskId, TPAllTaskId, voteTaskId, supplyTaskId, startStateUpdateTaskId, limitParticleTaskId, projectilePassTaskId, locationNotifyTaskId;
	
	public void initAllTask() {
		stopGameStartTask();
		stopDrawSkipTask();
		stopTPAllTask();
		stopVoteTask();
		stopSupplyTask();
		stopStartStateUpdateTask();
		stopLocationNotifyTask();
		stopLimitParticleTask();
	}
	
	public void runGameStartTask() {
		if(getGamerunTaskId() != 0) return;
		setGamerunTaskId(Bukkit.getScheduler().scheduleSyncRepeatingTask(HelpAbility.getInstance(), new GameStartTask(), 0, 20));
	}
	
	public void stopGameStartTask() {
		if(getGamerunTaskId() == 0) return;
		Bukkit.getScheduler().cancelTask(getGamerunTaskId());
		setGamerunTaskId(0);
	}
	
	public void runDrawSkipTask() {
		if(getDrawSkipTaskId() != 0) {
			stopDrawSkipTask();
		}
		api.getGameManager().setGameState(GameState.DRAWING);
		setDrawSkipTaskId(Bukkit.getScheduler().scheduleSyncRepeatingTask(HelpAbility.getInstance(), new DrawSkipTask(), 200, 20));
	}
	
	public void stopDrawSkipTask() {
		if(getDrawSkipTaskId() == 0) return;
		Bukkit.getScheduler().cancelTask(getDrawSkipTaskId());
		setDrawSkipTaskId(0);
	}
	
	public void runTPAllTask(int time, int TPAllTime) {
		if(getTPAllTaskId() != 0) {
			stopTPAllTask();
		}
		setTPAllTaskId(Bukkit.getScheduler().scheduleSyncRepeatingTask(HelpAbility.getInstance(), new TPAllTask(TPAllTime), time * 20, 20));
	}
	
	public void stopTPAllTask() {
		if(getTPAllTaskId() == 0) return;
		Bukkit.getScheduler().cancelTask(getTPAllTaskId());
		setTPAllTaskId(0);
	}
	
	public void runVoteTask(int time) {
		if(getVoteTaskId() != 0) {
			stopVoteTask();
		}
		setVoteTaskId(Bukkit.getScheduler().runTaskLaterAsynchronously(HelpAbility.getInstance(), new VoteTask(), time * 20).getTaskId());
	}
	
	public void stopVoteTask() {
		if(getVoteTaskId() == 0) return;
		Bukkit.getScheduler().cancelTask(getVoteTaskId());
		setVoteTaskId(0);
	}
	
	public void runSupplyTask() {
		if(getSupplyTaskId() != 0) {
			stopSupplyTask();
		}
		setSupplyTaskId(Bukkit.getScheduler().scheduleSyncRepeatingTask(HelpAbility.getInstance(), new SupplyTask(), api.getSupplyCreateInterval() * 20, api.getSupplyCreateInterval() * 20));
	}
	
	public void stopSupplyTask() {
		if(getSupplyTaskId() == 0) return;
		Bukkit.getScheduler().cancelTask(getSupplyTaskId());
		setSupplyTaskId(0);
	}
	
	public void runStartStateUpdateTask() {
		if(getStartStateUpdateTaskId() != 0) {
			stopStartStateUpdateTask();
		}
		setStartStateUpdateTaskId(Bukkit.getScheduler().scheduleSyncRepeatingTask(HelpAbility.getInstance(), new StartStateUpdateTask(), 20, 20));
	}
	
	public void stopStartStateUpdateTask() {
		if(getStartStateUpdateTaskId() == 0) return;
		Bukkit.getScheduler().cancelTask(getStartStateUpdateTaskId());
		setStartStateUpdateTaskId(0);
	}
	
	public void runLimitParticleTask() {
		if(getLimitParticleTaskId() != 0) {
			stopLimitParticleTask();
		}
		setLimitParticleTaskId(Bukkit.getScheduler().scheduleSyncRepeatingTask(HelpAbility.getInstance(), new LimitParticleTask(), 10, 10));
	}
	
	public void stopLimitParticleTask() {
		if(getLimitParticleTaskId() == 0) return;
		Bukkit.getScheduler().cancelTask(getLimitParticleTaskId());
		setLimitParticleTaskId(0);
	}
	
	public void runProjectilePassTask() {
		if(getProjectilePassTaskId() != 0) {
			stopProjectilePassTask();
		}
		setProjectilePassTaskId(Bukkit.getScheduler().scheduleSyncRepeatingTask(HelpAbility.getInstance(), new ProjectilePassTask(), 0, 1));
	}
	
	public void stopProjectilePassTask() {
		if(getProjectilePassTaskId() == 0) return;
		Bukkit.getScheduler().cancelTask(getProjectilePassTaskId());
		setProjectilePassTaskId(0);
	}
	
	public void runLocationNotifyTask() {
		if(getLocationNotifyTaskId() != 0) {
			stopLocationNotifyTask();
		}
		api.broadcast(ChatColor.DARK_GREEN, api.buildTime(api.getLocationNotifyInterval()) + "후 좌표가 공개됩니다.");
		setLocationNotifyTaskId(Bukkit.getScheduler().scheduleSyncRepeatingTask(HelpAbility.getInstance(), new LocationNotifyTask(), api.getLocationNotifyInterval() * 20, api.getLocationNotifyInterval() * 20));
	}
	
	public void stopLocationNotifyTask() {
		if(getLocationNotifyTaskId() == 0) return;
		Bukkit.getScheduler().cancelTask(getLocationNotifyTaskId());
		setLocationNotifyTaskId(0);
	}
	
}