package shadowuni.plugin.helpability.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;

import lombok.Getter;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class JoinEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	@Getter
	private final GamePlayer gamePlayer;
	@Getter
	private final boolean reconnect;
	@Getter
	private PlayerJoinEvent playerJoinEvent;
	
	public JoinEvent(GamePlayer gamePlayer, boolean reconnect, PlayerJoinEvent playerJoinEvent) {
		this.gamePlayer = gamePlayer;
		this.reconnect = reconnect;
		this.playerJoinEvent = playerJoinEvent;
	}
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}

}