package shadowuni.plugin.helpability.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.PlayerDeathEvent;

import lombok.Getter;
import shadowuni.plugin.helpability.category.DeathReason;

public class DeathEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	@Getter
	private final Player player;
	@Getter
	private final Player killer;
	@Getter
	private final DeathReason reason;
	@Getter
	private PlayerDeathEvent playerDeathEvent;
	
	public DeathEvent(Player player, Player killer, DeathReason reason, PlayerDeathEvent playerDeathEvent) {
		this.player = player;
		this.killer = killer;
		this.reason = reason;
		this.playerDeathEvent = playerDeathEvent;
	}
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
}