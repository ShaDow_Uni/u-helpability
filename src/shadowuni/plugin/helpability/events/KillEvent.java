package shadowuni.plugin.helpability.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.PlayerDeathEvent;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.helpability.category.KillType;

public class KillEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	@Setter
	@Getter
	private boolean cancelled = false;
	@Getter
	private boolean firstBlood;
	@Getter
	private final Player player;
	@Getter
	private final Player deathPlayer;
	@Setter
	@Getter
	private double killMoney;
	@Setter
	@Getter
	private double regularKillMoney;
	@Getter
	private KillType killType;
	@Getter
	private PlayerDeathEvent playerDeathEvent;
	
	public KillEvent(Player player, Player deathPlayer, boolean firstBlood, double killMoney, double regularKillMoney, KillType killType, PlayerDeathEvent playerDeathEvent) {
		this.firstBlood = firstBlood;
		this.player = player;
		this.deathPlayer = deathPlayer;
		this.killMoney = killMoney;
		this.regularKillMoney = regularKillMoney;
		this.killType = killType;
		this.playerDeathEvent = playerDeathEvent;
	}
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}

}