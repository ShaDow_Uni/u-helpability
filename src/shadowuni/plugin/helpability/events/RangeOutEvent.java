package shadowuni.plugin.helpability.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.helpability.objects.GameMap;

public class RangeOutEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	@Setter
	@Getter
	private boolean cancelled = false;
	
	@Getter
	private final Player player;
	@Getter
	private final GameMap map;
	
	public RangeOutEvent(Player player, GameMap map) {
		this.player = player;
		this.map = map;
	}
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
}