package shadowuni.plugin.helpability.events;

import java.util.List;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class WinEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	@Setter
	@Getter
	private boolean cancelled = false;
	
	@Getter
	private final List<GamePlayer> player;
	@Setter
	@Getter
	private double winMoney;
	
	public WinEvent(List<GamePlayer> player, double winMoney) {
		this.player = player;
		this.winMoney = winMoney;
	}
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
}