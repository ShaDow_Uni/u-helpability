package shadowuni.plugin.helpability.events;

import org.bukkit.command.CommandSender;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import lombok.Getter;
import lombok.Setter;

public class GameStopEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	@Setter
	@Getter
	private boolean cancelled = false;
	
	@Getter
	private final CommandSender stopper;
	
	public GameStopEvent(CommandSender stopper) {
		this.stopper = stopper;
	}
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
}