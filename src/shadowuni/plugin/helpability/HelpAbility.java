package shadowuni.plugin.helpability;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import me.confuser.barapi.BarAPI;
import shadowuni.plugin.bungeechat.BungeeChatAPI;
import shadowuni.plugin.bungeeparty.BungeeParty;
import shadowuni.plugin.helpability.category.GameCommand;
import shadowuni.plugin.helpability.commands.CommandHandler;
import shadowuni.plugin.helpability.listeners.AutoGameListener;
import shadowuni.plugin.helpability.listeners.BungeeListener;
import shadowuni.plugin.helpability.listeners.BungeePartyListener;
import shadowuni.plugin.helpability.listeners.ControlListener;
import shadowuni.plugin.helpability.listeners.GamePlayerListener;
import shadowuni.plugin.helpability.listeners.KitListener;
import shadowuni.plugin.helpability.listeners.MapListener;
import shadowuni.plugin.helpability.listeners.MessageListener;
import shadowuni.plugin.helpability.listeners.WatchListener;
import shadowuni.plugin.helpability.objects.GamePlayer;
import shadowuni.plugin.helpability.tasks.PartyParticleTask;
import shadowuni.plugin.prefixer.PrefixerAPI;

public class HelpAbility extends JavaPlugin {
	
	@Getter
	private static HelpAbility instance;
	@Getter
	private static String version;
	@Getter
	private static HelpAbilityAPI api = new HelpAbilityAPI();
	
	public void onEnable() {
		instance = this;
		version = getDescription().getVersion() + (api.isTest() ? " Test" : "");
		api.getLibrary().registerPrefix(getClass().getPackage().getName(), api.pluginPrefix);
		if(!api.isTest() && api.getLibrary().getUpdateManager().updatePlugin(this, true, true).equals("success")) return;
		api.init();
		loadConfig();
		api.getFileManager().loadSpawn();//s
		api.getFileManager().loadAllMap();
		api.getFileManager().loadKit();
		api.getFileManager().loadRankItem();
		api.getFileManager().loadSupply();
		registerPlugin();
		registerListeners();
		getCommand("ha").setExecutor(new CommandHandler());
		for(Player p : Bukkit.getOnlinePlayers()) {
			GamePlayer gp = new GamePlayer();
			gp.setPlayer(p);
			gp.setOnline(true);
			api.getPlayerManager().setGamePlayer(p, gp);
		}
		if(api.isUseBungeecord()) {
		    Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(HelpAbility.getInstance(), "BungeeCord");
		    Bukkit.getServer().getMessenger().registerIncomingPluginChannel(HelpAbility.getInstance(), "BungeeCord", new MessageListener());
		}
		if(api.isUsePartyParticle()) {
			Bukkit.getScheduler().scheduleSyncRepeatingTask(HelpAbility.getInstance(), new PartyParticleTask(), 0, 2);
		}
		api.log("플러그인이 활성화되었습니다. (v" + getVersion() + ")");
	}
	
	public void onDisable() {
		api.getBarApi().onDisable();
		api.log("플러그인이 비활성화되었습니다. (v" + getVersion() + ")");
	}
	
	public void registerListeners() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new AutoGameListener(), this);
		pm.registerEvents(new GamePlayerListener(), this);
		pm.registerEvents(new ControlListener(), this);
		pm.registerEvents(new BungeeListener(), this);
		pm.registerEvents(new KitListener(), this);
		pm.registerEvents(new MapListener(), this);
		pm.registerEvents(new WatchListener(), this);
		pm.registerEvents(new BarAPI(), this);
	}
	
	public void loadConfig() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		// 게임
		api.startPlayerCount = getConfig().getInt("게임.시작 인원");
		api.skipCount = getConfig().getInt("게임.능력 강제 확정 시간");
		api.winMinCount = getConfig().getInt("게임.우승 최소 시간");
		api.voteCount = getConfig().getInt("게임.시작 투표 타임아웃");
		api.quitDeathHealth = getConfig().getInt("게임.퇴장 시 탈락 체력");
		api.allowNatureDeath = !getConfig().getBoolean("게임.자연사 탈락");
		api.useBar = getConfig().getBoolean("게임.상단 바 사용");
		api.useRankItem = getConfig().getBoolean("게임.등급 아이템 사용");
		// 명령어
		GameCommand.START = getConfig().getString("명령어.게임 시작");
		GameCommand.STOP = getConfig().getString("명령어.게임 중지");
		GameCommand.SKIP = getConfig().getString("명령어.능력 강제 확정");
		//MOTD
		api.waitMotd = api.replaceColor(getConfig().getString("MOTD.대기"));
		api.gameMotd = api.replaceColor(getConfig().getString("MOTD.게임"));
		// 텔레포트
		api.useTPAll = getConfig().getBoolean("텔레포트.사용");
		api.TPAllCount = getConfig().getInt("텔레포트.시간");
		api.useTPAllRepeat = getConfig().getBoolean("텔레포트.반복");
		api.TPAllRepeatCount = getConfig().getInt("텔레포트.반복 시간");
		// 게임 시작 전 조작
		api.useInvincbility = getConfig().getBoolean("게임 시작 전 조작.무적");
		api.usePvpProtect = getConfig().getBoolean("게임 시작 전 조작.PVP 방지");
		api.useBlockProtect = getConfig().getBoolean("게임 시작 전 조작.블럭 보호");
		api.useCmdProtect = getConfig().getBoolean("게임 시작 전 조작.명령어 금지");
		api.exceptionCommands = getConfig().getStringList("게임 시작 전 조작.예외 명령어");
		//보상
		api.killMoney = getConfig().getDouble("보상.킬");
		api.winMoney = getConfig().getDouble("보상.우승");
		api.assistMoney = getConfig().getDouble("보상.어시스트");
		api.firstMoney = getConfig().getDouble("보상.퍼스트 블러드");
		api.doubleMoney = getConfig().getDouble("보상.더블 킬");
		api.tripleMoney = getConfig().getDouble("보상.트리플 킬");
		api.quadraMoney = getConfig().getDouble("보상.쿼드라 킬");
		api.pentaMoney = getConfig().getDouble("보상.펜타 킬");
		// 어시스트
		api.useAssist = getConfig().getBoolean("어시스트.사용");
		api.assistCount = getConfig().getInt("어시스트.시간");
		// 연속킬
		api.useFirst = getConfig().getBoolean("연속 킬.퍼스트 블러드 사용");
		api.useDouble = getConfig().getBoolean("연속 킬.더블 킬 사용");
		api.useTriple = getConfig().getBoolean("연속 킬.트리플 킬 사용");
		api.useQuadra = getConfig().getBoolean("연속 킬.쿼드라 킬 사용");
		api.usePenta = getConfig().getBoolean("연속 킬.펜타 킬 사용");
		api.doubleCount = getConfig().getInt("연속 킬.더블 킬 시간");
		api.tripleCount = getConfig().getInt("연속 킬.트리플 킬 시간");
		api.quadraCount = getConfig().getInt("연속 킬.쿼드라 킬 시간");
		api.pentaCount = getConfig().getInt("연속 킬.펜타 킬 시간");
		// 파티
		api.useParty = getConfig().getBoolean("파티.사용");
		api.maxPartyPlayerCount = getConfig().getInt("파티.최대 인원");
		api.usePartyParticle = getConfig().getBoolean("파티.파티클 사용");
		api.partyParticle = getConfig().getString("파티.파티클");
		// 관전
		api.useWatchCmdProtect = getConfig().getBoolean("관전.명령어 금지");
		api.exceptionWatchCommands = getConfig().getStringList("관전.예외 명령어");
		// 보급품
		api.useSupply = getConfig().getBoolean("보급품.사용");
		api.useSupplyFirework = getConfig().getBoolean("보급품.폭죽 사용");
		api.supplyCreateInterval = getConfig().getInt("보급품.생성 간격");
		//좌표 알림
		api.useLocationNotifyMessage = getConfig().getBoolean("좌표 알림.메시지 사용");
		api.useLocationNotifyFirework = getConfig().getBoolean("좌표 알림.폭죽 사용");
		api.locationNotifyInterval = getConfig().getInt("좌표 알림.간격");
		// 재접속
		api.allowReconnect = getConfig().getBoolean("재접속.허용");
		api.reconnectTime = getConfig().getInt("재접속.허용 시간");
		// 맵 제한
		api.useAutoMapLimit = getConfig().getBoolean("맵 제한.맵 제한 사용");
		api.autoMapLimitRange = getConfig().getInt("맵 제한.맵 제한 범위(반지름)");
		api.useAutoTPAllMapLimit = getConfig().getBoolean("맵 제한.티피올 맵 제한 사용");
		api.autoTPAllMapLimitRange = getConfig().getInt("맵 제한.티피올 맵 제한 범위(반지름)");
		api.useMapLimitParticle = getConfig().getBoolean("맵 제한.파티클 사용");
		api.mapLimitParticle = getConfig().getString("맵 제한.파티클");
		// 번지코드
		api.useBungeecord = getConfig().getBoolean("번지코드.사용");
		api.lobbyServer = getConfig().getString("번지코드.로비");
		api.log("설정을 불러왔습니다.");
	}
	
	public void registerPlugin() {
		if(api.useBungeeChat = (Bukkit.getPluginManager().getPlugin("U-BungeeChat") != null)) {
			api.chatApi = new BungeeChatAPI();
			api.log("U-BungeeChat 플러그인과 연동되었습니다.");
		}
		if(api.usePrefixer = (Bukkit.getPluginManager().getPlugin("U-Prefixer") != null)) {
			api.prefixerApi = new PrefixerAPI();
			api.log("U-Prefixer 플러그인과 연동되었습니다.");
		}
		if(api.useBungeeParty = (Bukkit.getPluginManager().getPlugin("U-BungeeParty") != null)) {
			api.partyApi = BungeeParty.getApi();
			Bukkit.getPluginManager().registerEvents(new BungeePartyListener(), this);
			api.log("U-BungeeParty 플러그인과 연동되었습니다.");
		}
		if(api.useBar) {
			try {
				if(Bukkit.getPluginManager().getPlugin("BitAbility") != null) {
					api.useBitAbility = true;
					api.log("비트 능력자 플러그인과 연동되었습니다.");
				}
			} catch(Exception e) {}
			try {
				if(Bukkit.getPluginManager().getPlugin("PhysicalFighters") != null) {
					api.useDyeAbility = true;
					api.log("염료 능력자 플러그인과 연동되었습니다.");
				}
			} catch(Exception e) {}
		}
	}
	
    @SuppressWarnings("null")
	public void dp() {
        ArrayList<Integer> list = null;
        list.stream().filter(num -> num % 2 == 0);
    }

}
