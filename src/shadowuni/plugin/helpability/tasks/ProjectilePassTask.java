package shadowuni.plugin.helpability.tasks;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.scheduler.BukkitRunnable;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class ProjectilePassTask extends BukkitRunnable {
	
	private HelpAbilityAPI api = HelpAbility.getApi();

	@Override
	public void run() {
		List<GamePlayer> watch = api.getPlayerManager().getWatchPlayers();
		if(watch.size() < 1) return;
		for(GamePlayer gp : watch) {
			Player p = gp.getPlayer();
			for(Entity e : p.getWorld().getEntities()) {
				if(!(e instanceof Projectile)) continue;
				if(!isOverLap(e.getLocation(), p.getLocation())) continue;
				p.teleport(getEmptyLocation(p.getLocation()));
				api.msg(p, ChatColor.RED, "게임 중인 플레이어에게 방해되어 강제로 텔레포트되었습니다.");
			}
		}
	}
	
	private boolean isOverLap(Location loc, Location player) {
		return Math.pow(loc.getY() - player.getY(), 2) < 9 && Math.pow(loc.getX() - player.getX(), 2) < 9 && Math.pow(loc.getZ() - player.getZ(), 2) < 9;
	}
	
	private Location getEmptyLocation(Location loc) {
		double farDistance = 0;
		Location farLoc = null;
		for(int i = 2; i < 5; i++) {
			for(int j = 2; j < 5; j++) {
				for(int k = 0; k < 3; k++) {
					if(loc.getWorld().getBlockTypeIdAt((int) loc.getX() + i, (int) loc.getY() + k, (int) loc.getZ() + j) == 0 && loc.getWorld().getBlockTypeIdAt((int) loc.getX() + i, (int) loc.getY() + k + 1, (int) loc.getZ() + j) == 0) {
						Location tLoc = new Location(loc.getWorld(), loc.getX() + i, loc.getY() + k, loc.getZ() + j, loc.getYaw(), loc.getPitch());
						double tDistance = Math.pow(loc.getX() - tLoc.getX(), 2) + Math.pow(loc.getZ() - tLoc.getZ(), 2);
						if(tDistance > farDistance) {
							farDistance = tDistance;
							farLoc = tLoc;
						}
					}
				}
			}
		}
		if(farLoc == null || farDistance < 6) return api.getGameManager().isGameStarted() ? (api.getGameManager().isTeleportedAll() ? api.getMapManager().getPlayingMap().getTPAllLocation() : api.getMapManager().getPlayingMap().getMapLocation()) : api.getMapManager().getSpawnMap().getMapLocation();
		return farLoc;
	}
	
}