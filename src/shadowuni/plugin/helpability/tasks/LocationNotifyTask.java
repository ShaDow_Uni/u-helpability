package shadowuni.plugin.helpability.tasks;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class LocationNotifyTask extends BukkitRunnable {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@Override
	public void run() {
		for(GamePlayer gp : api.getPlayerManager().getGamePlayers()) {
			Location location = gp.getPlayer().getLocation();
			if(api.isUseLocationNotifyMessage()) {
				api.broadcast(ChatColor.DARK_GREEN, gp.getDisplayName() + " 님의 좌표: (X: " + Math.round(location.getX()) + ", Y: " + Math.round(location.getY()) + ", Z: " + Math.round(location.getZ()) + ")");
			}
			if(api.isUseLocationNotifyFirework()) {
				api.spawnFirework(location);
			}
		}
		api.getTaskManager().runLocationNotifyTask();
	}
	
}