package shadowuni.plugin.helpability.tasks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class TPAllTask extends BukkitRunnable {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	private int count;
	private int TPAllTime;
	
	public TPAllTask(int TPAllTime) {
		count = -1;
		this.TPAllTime = TPAllTime;
	}
	
	public void run() {
		count++;
		if(getCount() >= TPAllTime) {
			api.getGameManager().setTeleportedAll(true);
			api.getGameManager().setTPAllCount(api.getGameManager().getTPAllCount() + 1);
			for(Player ap : Bukkit.getOnlinePlayers()) {
				if(ap.getVehicle() != null) {
					ap.leaveVehicle();
				}
				ap.teleport(api.getMapManager().getPlayingMap().getTPAllLocation());
				for(GamePlayer asp : api.getPlayerManager().getGamePlayers()) {
					ap.showPlayer(asp.getPlayer());
				}
			}
			api.getBarApi().setMessage(api.buildmsg(ChatColor.DARK_GREEN, "모두 텔레포트되었습니다."), 10);
			api.broadcast(ChatColor.DARK_GREEN, "모두 텔레포트되었습니다.");
			if(api.isUseTPAllRepeat()) {
				api.getTaskManager().runTPAllTask(10, api.getTPAllRepeatCount());
			}
			return;
		}
		api.getBarApi().setMessage(api.buildmsg(ChatColor.DARK_GREEN, api.buildTime(TPAllTime - count) + " 후 모두 텔레포트됩니다."), (float) 100 - (float) getCount() / (float) TPAllTime * 100);
		if(getCount() == 0 || (getCount() > TPAllTime - 10 && getCount() < TPAllTime)) {
			api.broadcast(ChatColor.DARK_GREEN, api.buildTime(TPAllTime - count) + " 후 모두 텔레포트됩니다.");
			return;
		}
	}
	
	public int getCount() {
		return count;
	}
	
}
