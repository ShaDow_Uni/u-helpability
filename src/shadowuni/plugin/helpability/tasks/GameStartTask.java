package shadowuni.plugin.helpability.tasks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import lombok.Getter;
import shadowuni.plugin.bungeeparty.objects.PartyPlayer;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.category.GameCommand;
import shadowuni.plugin.helpability.category.GameState;
import shadowuni.plugin.helpability.objects.GameMap;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class GameStartTask extends BukkitRunnable {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@Getter
	private int count;
	
	public GameStartTask() {
		count = 0;
	}
	
	public void run() {
		count++;
		if(getCount() > 0 && getCount() < 5) {
			String msg = "잠시 후 게임이 시작됩니다.";
			api.getBarApi().setMessage(api.buildmsg(ChatColor.DARK_GREEN, msg), 100 - getCount() * 10f);
			if(getCount() == 1) {
				api.broadcast(ChatColor.DARK_GREEN, msg);
			}
			return;
		} else if(getCount() > 4 && getCount() < 10) {
			String msg = 10 - getCount() + "초 후 게임이 시작됩니다.";
			api.getBarApi().setMessage(api.buildmsg(ChatColor.DARK_GREEN, msg), 100 - getCount() * 10f);
			api.broadcast(ChatColor.DARK_GREEN, msg);
			return;
		} else if(getCount() > 9){
			if(api.getMapManager().getPlayingMap() == null) {
				GameMap map = api.getMapManager().getRandomMap();
				if(map == null) {
					api.broadcast(ChatColor.RED, "설정된 맵이 없어 게임이 종료됩니다.");
					api.getGameManager().GameStop(null);
					return;
				}
				api.getMapManager().setPlayingMap(map);
			}
			GameMap map = api.getMapManager().getPlayingMap();
			for(Player ap : Bukkit.getOnlinePlayers()) {
				if(ap.getVehicle() != null) {
					ap.leaveVehicle();
				}
				if(map.isRandomTeleport()) {
					if(api.isUseBungeeParty()) {
						PartyPlayer pp = api.getPartyApi().getPlayerManager().getPartyPlayer(ap);
						if(pp != null && pp.hasParty()) {
							if(!pp.getParty().getOwnerName().equals(ap.getName())) continue;
							Location location = map.getRandomLocation();
							for(Player pap : pp.getParty().getOnlinePlayers()) {
								pap.teleport(location);
							}
							continue;
						}
					}
					ap.teleport(map.getRandomLocation());
				} else {
					ap.teleport(map.getMapLocation());
				}
				for(GamePlayer asp : api.getPlayerManager().getGamePlayers()) {
					if(asp.getPlayer() == null || asp.isWatchMode()) continue;
					ap.showPlayer(asp.getPlayer());
				}
			}
			api.getGameManager().setGameState(GameState.DRAWING);
			String msg = "'" + map.getName() + "' 맵으로 이동되었습니다.";
			api.getBarApi().setMessage(api.buildmsg(ChatColor.DARK_GREEN, msg));
			api.broadcast(ChatColor.DARK_GREEN, msg);
			Player tempplayer = Bukkit.getOnlinePlayers()[0];
			if(tempplayer.isOp()) {
				Bukkit.dispatchCommand(tempplayer, GameCommand.START);
			} else {
				tempplayer.setOp(true);
				Bukkit.dispatchCommand(tempplayer, GameCommand.START);
				tempplayer.setOp(false);
			}
			api.getTaskManager().runDrawSkipTask();
			api.getTaskManager().stopGameStartTask();
			return;
		}
	}
	
}