package shadowuni.plugin.helpability.tasks;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.objects.GameMap;

public class LimitParticleTask extends BukkitRunnable {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	public void run() {
		if(!api.getGameManager().isGameStarted()) return;
		GameMap map = api.getMapManager().getPlayingMap();
		for(Player p : Bukkit.getOnlinePlayers()) {
			Location pl = p.getLocation();
			double y = pl.getY();
			for(Location l : map.getPacketLocations()) {
				if(l.distance(pl) > 15) continue;
				l.setY(y);
				api.spawnParticles(p, api.getMapLimitParticle(), l, 0, 0, 1, 0, 2);
			}
		}
	}

}