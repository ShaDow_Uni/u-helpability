package shadowuni.plugin.helpability.tasks;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;

public class SupplyTask extends BukkitRunnable {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	public void run() {
		Location location = api.getSupplyManager().createRandomSupplyAtRandomLocation(api.getMapManager().getPlayingMap());
		api.broadcast(ChatColor.DARK_GREEN, "(X: " + location.getX() + ", Y: " + location.getY() + ", Z: " + location.getZ() + ") 에 보급품이 생성되었습니다.");
	}

}
