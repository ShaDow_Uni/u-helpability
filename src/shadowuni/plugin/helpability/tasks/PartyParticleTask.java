package shadowuni.plugin.helpability.tasks;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.objects.Party;

public class PartyParticleTask implements Runnable {
	
	private HelpAbilityAPI api = new HelpAbilityAPI();
	
	public void run() {
		for(Party party : api.getPartyManager().getPartys().values()) {
			for(Player p : party.getPlayers()) {
				if(p == null) continue;
				for(Player pp : party.getPlayers()) {
					if(pp == null) continue;
					if(p.getName().equals(pp.getName())) continue;
					Location l = pp.getLocation();
					l.setY(l.getY() + 2.5);
					api.spawnParticles(p, api.getPartyParticle(), l, 0, 0, 0, 0, 1);
				}
			}
		}
	}

}