package shadowuni.plugin.helpability.tasks;

import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;

public class VoteTask extends BukkitRunnable {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	public void run() {
		if(api.getGameManager().isGameStarted()) return;
		api.broadcast(ChatColor.RED, "투표 시간이 초과하여 투표가 부결되었습니다.");
		api.getVoteManager().stopVote();
	}

}