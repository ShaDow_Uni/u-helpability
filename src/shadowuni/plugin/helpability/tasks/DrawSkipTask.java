package shadowuni.plugin.helpability.tasks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import lombok.Getter;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.category.GameCommand;

public class DrawSkipTask extends BukkitRunnable {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@Getter
	private int count;
	
	public DrawSkipTask() {
		count = -1;
	}
	
	public void run() {
		count++;
		if(getCount() == 0) {
			api.broadcast(ChatColor.DARK_GREEN, api.buildTime(api.getSkipCount() - getCount()) + " 후 능력이 강제로 확정됩니다.");
		} else if(getCount() >= api.getSkipCount() || api.getGameManager().isAbilityGameStarted()) {
			api.getBarApi().clearBar();
			if(!api.getGameManager().isAbilityGameStarted()) {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), GameCommand.SKIP);
			}
			if(api.isUseTPAll()) {
				api.getTaskManager().runTPAllTask(10, api.getTPAllCount());
			}
			if(api.isUseLocationNotifyMessage() || api.isUseLocationNotifyFirework()) {
				api.getTaskManager().runLocationNotifyTask();
			}
			api.getTaskManager().stopDrawSkipTask();
			return;
		} else {
			api.getBarApi().setMessage(api.buildmsg(ChatColor.DARK_GREEN, api.buildTime(api.getSkipCount() - getCount()) + " 후 능력이 강제로 확정됩니다."), (float) 100 - (float) getCount() / (float) api.getSkipCount() * 100);
			return;
		}
	}
	
}