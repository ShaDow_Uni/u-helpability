package shadowuni.plugin.helpability.tasks;

import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.category.GameState;

public class StartStateUpdateTask  extends BukkitRunnable {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	private int count;
	
	public StartStateUpdateTask() {
		count = 0;
	}
	
	public void run() {
		if(api.getGameManager().isAbilityGameStarted() || count == 1) {
			count++;
		} else if(count > 1) {
			api.getGameManager().setGameState(GameState.PLAYING);
			if(api.isUseRankItem()) {
				api.getRankItemManager().giveItemAll();
				api.broadcast(ChatColor.DARK_GREEN, "아이템이 지급되었습니다!");
			}
			api.getTaskManager().stopStartStateUpdateTask();
		} else if(!api.getGameManager().getGameState().equals(GameState.STARTING) && api.getGameManager().isAbilityGameStarting()) {
			api.getGameManager().setGameState(GameState.STARTING);
		}
	}

}