package shadowuni.plugin.helpability.listeners;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;

public class MessageListener implements PluginMessageListener {
	
	private HelpAbilityAPI api = HelpAbility.getApi();

	public void onPluginMessageReceived(String channel, Player arg1, byte[] message) {
		if(!channel.equals("BungeeCord")) return;
		ByteArrayDataInput in = ByteStreams.newDataInput(message);
		String task = in.readUTF();
		if(task.equals("PlayerList")) {
			in.readUTF();
			api.setBungeePlayerList(in.readUTF().split(", "));
		} else if(task.equals("GetServer")) {
			api.setServerName(in.readUTF());
		}
	}
	

}
