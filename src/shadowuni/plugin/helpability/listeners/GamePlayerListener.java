package shadowuni.plugin.helpability.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.server.ServerListPingEvent;

import Xeon.VisualAbility.MainModule.AbilityBase;
import Xeon.VisualAbility.MajorModule.AbilityList;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.category.DeathReason;
import shadowuni.plugin.helpability.category.GameState;
import shadowuni.plugin.helpability.category.KillType;
import shadowuni.plugin.helpability.events.DeathEvent;
import shadowuni.plugin.helpability.events.JoinEvent;
import shadowuni.plugin.helpability.events.KillEvent;
import shadowuni.plugin.helpability.objects.GamePlayer;
import shadowuni.plugin.prefixer.objects.PrefixPlayer;

@SuppressWarnings("deprecation")
public class GamePlayerListener implements Listener {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@EventHandler
	public void onPing(ServerListPingEvent e) {
		String motd = null;
		if(api.getGameManager().isGameStarted()) {
			motd = api.getGameMotd();
		} else {
			motd = api.getWaitMotd();
		}
		e.setMotd(motd);
	}
	
	@EventHandler (priority = EventPriority.LOW)
	public void onLogin(PlayerLoginEvent e) {
		Player p = e.getPlayer();
		if(!api.getGameManager().isGameStarted() || api.isAdmin(p, false) || p.hasPermission("helpability.watchmode")) return;
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(gp != null && !gp.isDead() && System.currentTimeMillis() - gp.getQuitTime() <= api.getReconnectTime() * 1000) return;
		e.disallow(Result.KICK_WHITELIST, "게임 중에는 접속이 불가능합니다.");
	}
	
	@EventHandler (priority = EventPriority.LOW)
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		
		// 플레이어 생성
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(gp == null) {
			gp = new GamePlayer();
			gp.setPlayer(p);
		}
		gp.setOnline(true);
		api.getPlayerManager().setGamePlayer(p, gp);
		
		// 게임 중 접속
		if(!(api.getGameManager().getGameState().equals(GameState.WAITING) || api.getGameManager().getGameState().equals(GameState.PREPARING))) {
			if(gp.isDead() || gp.isWatchMode() || System.currentTimeMillis() - gp.getQuitTime() > api.getReconnectTime() * 1000) {
				if(!gp.isWatchMode() && System.currentTimeMillis() - gp.getQuitTime() <= api.getReconnectTime() * 1000) {
					api.msg(p, ChatColor.BLUE, "재접속 시간이 지나 관전모드로 전환되었습니다.");
				}
				if(api.isUseBitAbility()) {
					for(AbilityBase ab : AbilityList.AbilityList) {
						if(!ab.PlayerCheck(p)) continue;
						ab.SetPlayer(null, false);
					}
				} else if(api.isUseDyeAbility()) {
					for(Physical.Fighters.MainModule.AbilityBase ab : Physical.Fighters.MajorModule.AbilityList.AbilityList) {
						if(!ab.PlayerCheck(p)) continue;
						ab.SetPlayer(null, false);
					}
				}
				gp.setWatchMode(true);
				gp.hidePlayer();
				p.setGameMode(GameMode.ADVENTURE);
				p.setAllowFlight(true);
				p.setFlying(true);
				Bukkit.getScheduler().scheduleAsyncDelayedTask(HelpAbility.getInstance(), () -> {
					p.teleport(api.getGameManager().isGameStarted() ? (api.getGameManager().isTeleportedAll() ? api.getMapManager().getPlayingMap().getTPAllLocation() : api.getMapManager().getPlayingMap().getMapLocation()) : api.getMapManager().getSpawnMap().getMapLocation());
					api.msg(p, ChatColor.BLUE, "맵으로 이동되었습니다.");
				}, 4);
			}
		} else {
			Bukkit.getScheduler().scheduleAsyncDelayedTask(HelpAbility.getInstance(), () -> {
				p.teleport(api.getMapManager().getSpawnMap().getMapLocation());
			}, 4);
		}
		
		// JoinEvent
		Bukkit.getPluginManager().callEvent(new JoinEvent(gp, false, e));
	}
	
	@EventHandler (priority = EventPriority.LOW)
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		gp.setOnline(false);
		if(gp.isWatchMode()) {
			gp.showPlayer();
			p.setAllowFlight(false);
			p.setFlying(false);
			p.setGameMode(GameMode.SURVIVAL);
		}
		if(!api.getGameManager().isGameStarted()) return;
		if(!api.getGameManager().getGameState().equals(GameState.END) && !gp.isDead() && !gp.isWatchMode() && p.getHealth() >= 0 && p.getHealth() < api.getQuitDeathHealth()) {
			p.setHealth(0);
			Bukkit.getPluginManager().callEvent(new DeathEvent(p, p.getKiller(), DeathReason.LOW_HEALTH, null));
			api.broadcast(ChatColor.DARK_GREEN, gp.getDisplayName() + "님께서 낮은 체력으로 퇴장하여 탈락 처리되었습니다.");
		}
		api.getPlayerManager().setGamePlayer(p, gp);
	}
	
	@EventHandler
	public void onKill(PlayerDeathEvent e) {
		if(!api.getGameManager().isGameStarted()) return;
		Player p = e.getEntity();
		GamePlayer dp = api.getPlayerManager().getGamePlayer(p);
		if(dp.isDead() || dp.isWatchMode()) return;
		Player killer = p.getKiller();
		if(killer instanceof Projectile) {
			Projectile pj = (Projectile) p.getKiller();
			killer = (Player) pj.getShooter();
		}
		if(!(killer instanceof Player) || killer.equals(p)) return;
		GamePlayer gp = api.getPlayerManager().getGamePlayer(killer);
		KillType lastKillType = gp.getLastKillType();
		KillType killType = KillType.NORMAL;
		long currentTime = System.currentTimeMillis();
		double killMoney = api.getKillMoney();
		double regularKillMoney = 0;
		if(gp.getLastKillTime() != 0) {
			long time = (currentTime - gp.getLastKillTime()) / 1000;
			if(lastKillType.equals(KillType.NORMAL) && api.getDoubleCount() >= time) {
				regularKillMoney = api.getDoubleMoney();
				killType = KillType.DOUBLE;
			} else if(lastKillType.equals(KillType.DOUBLE) && api.getTripleCount() >= time) {
				regularKillMoney = api.getTripleMoney();
				killType = KillType.TRIPLE;
			} else if(lastKillType.equals(KillType.TRIPLE) && api.getQuadraCount() >= time) {
				regularKillMoney = api.getQuadraMoney();
				killType = KillType.QUADRA;
			} else if(lastKillType.equals(KillType.QUADRA) && api.getPentaCount() >= time) {
				regularKillMoney = api.getPentaMoney();
				killType = KillType.PENTA;
			}
		}
		KillEvent event = new KillEvent(killer, p, api.isFirstBlood(), killMoney, regularKillMoney, killType, e);
		Bukkit.getPluginManager().callEvent(event);
		if(event.isCancelled()) return;
		if(api.isUseAssist()) {
			for(String name : dp.getLastHitTimes().keySet()) {
				if(killer.getName().equalsIgnoreCase(name) || (System.currentTimeMillis() - dp.getLastHitTime(name)) > api.getAssistCount() * 1000) continue;
				api.getLibrary().getVault().giveMoney(name, api.getAssistMoney());
				Player ap = Bukkit.getPlayer(name);
				if(ap == null) continue;
				api.msg(ap, ChatColor.DARK_GREEN, "'" + gp.getDisplayName() + "'님을 도와 '" + dp.getDisplayName() + "' 님을 잡아 " + api.getAssistMoney() + "원을 획득했습니다!");
			}
		}
		api.getLibrary().getVault().giveMoney(killer, event.getKillMoney());
		api.msg(killer, ChatColor.DARK_GREEN, "'" + dp.getDisplayName() + "' 님을 잡아 " + killMoney + "원을 획득했습니다!");
		if(api.isFirstBlood()) {
			api.broadcast(ChatColor.DARK_RED, gp.getDisplayName() + "님께서 퍼스트 블러드!");
			api.getLibrary().getVault().giveMoney(killer, api.getFirstMoney());
			api.msg(killer, ChatColor.DARK_RED, "첫 번째 킬로 인해 " + api.getFirstMoney() + "원을 추가 지급받았습니다!");
			api.setFirstBlood(false);
		}
		gp.setLastKillTime(currentTime);
		gp.setLastKillType(killType);
		if(killType.equals(KillType.NORMAL)) return;
		api.broadcast(ChatColor.DARK_RED, gp.getDisplayName() + "님께서 " + killType.getText() + "!");
		if(regularKillMoney != 0) {
			api.getLibrary().getVault().giveMoney(killer, regularKillMoney);
			api.msg(killer, ChatColor.DARK_RED, "연속 킬로 인해 " + regularKillMoney + "원을 추가 지급받았습니다!");
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onDeath(PlayerDeathEvent e) {
		Player p = e.getEntity();
		if(!api.getGameManager().isGameStarted()) return;
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if((p.getKiller() == null || !(p.getKiller() instanceof Player)) && api.isAllowNatureDeath() && !gp.isDead()) {
			Bukkit.getPluginManager().callEvent(new DeathEvent(p, p.getKiller(), DeathReason.NATURE, e));
			api.broadcast(ChatColor.DARK_AQUA, gp.getDisplayName() + "님께서 자연사하여 다시 부활했습니다.");
			return;
		}
		gp.setDead(true);
		Bukkit.getPluginManager().callEvent(new DeathEvent(p, p.getKiller(), DeathReason.PLAYER, e));
		if(api.getGameManager().finish()) {
			api.shutdown(13);
		} else if(!api.getGameManager().getGameState().equals(GameState.END) && api.getPlayerManager().getGamePlayers().size() < 2) {
			for(Player ap : Bukkit.getOnlinePlayers()) {
				ap.teleport(api.getMapManager().getSpawnMap().getMapLocation());
			}
			api.getGameManager().GameStop(null);
			api.broadcast(ChatColor.DARK_GREEN, "인원 수가 부족하여 게임이 중단됩니다!");
		}
		if(api.isUseBungeecord()) return;
		p.kickPlayer("패배하셨습니다. 다음 기회를 노려보세요!");
	}
	
	@EventHandler (priority=EventPriority.HIGHEST)
	public void onRespawn(PlayerRespawnEvent e) {
		Player p = e.getPlayer();
		Location map;
		if(!api.getGameManager().isGameStarted()) {
			map = api.getMapManager().getSpawnMap().getMapLocation();
			api.msg(p, ChatColor.DARK_GREEN, "스폰으로 이동되었습니다.");
		} else if(api.getGameManager().isTeleportedAll()) {
			map = api.getMapManager().getPlayingMap().getTPAllLocation();
			api.msg(p, ChatColor.DARK_GREEN, "맵으로 이동되었습니다.");
		} else {
			map = api.getMapManager().getPlayingMap().getMapLocation();
			api.msg(p, ChatColor.DARK_GREEN, "맵으로 이동되었습니다.");
		}
		e.setRespawnLocation(map);
		p.teleport(map);
	}
	
	@EventHandler (priority = EventPriority.HIGH)
	public void onChat(PlayerChatEvent e) {
		Player p = e.getPlayer();
		String msg = e.getMessage();
		if(api.isAdmin(p, false)) {
			msg = api.replaceColor(msg);
		}
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		String prefix = "";
		if(api.isUsePrefixer()) {
			PrefixPlayer pp = api.getPrefixerApi().getPlayerManager().getPrefixPlayer(p);
			if(pp != null && pp.getMainPrefix() != null) {
				prefix = pp.getMainPrefix();
			}
		}
		String pname = (prefix.equals("") ? "" : ChatColor.WHITE + prefix + " ") + api.replaceColor((api.getLibrary().getVault().getChat().getPlayerPrefix(p) == null ? "" : ChatColor.WHITE + api.getLibrary().getVault().getChat().getPlayerPrefix(p)) + ChatColor.WHITE + gp.getDisplayName());
		if(gp.isPartyChat()) {
			if(!gp.hasParty()) {
				gp.setPartyChat(false);
				api.msg(p, ChatColor.RED, "파티에 참여 중이지 않아 파티 채팅이 해제되었습니다.");
				return;
			}
			api.getPartyManager().sendPartyChat(p, msg);
			e.setCancelled(true);
			return;
		}
		String pf = "◇ ";
		ChatColor c;
		if(gp.isWatchMode()) {
			pf += "관 전";
			c = ChatColor.GRAY;
		} else if(api.getGameManager().isGameStarted()) {
			pf += "게임 중";
			c = ChatColor.RED;
		} else {
			pf += "대기 중";
			c = ChatColor.AQUA;
		}
		msg = c + pf + " :: " + pname + c + " > " + ChatColor.WHITE + "%2$s";
		e.setFormat(msg);
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onHit(EntityDamageByEntityEvent e) {
		if(!(e.getEntity() instanceof Player)) return;
		Player p = null;
		if(e.getDamager() instanceof Projectile) {
			Projectile pj = (Projectile) e.getDamager();
			if(!(pj.getShooter() instanceof Player)) return;
			p = (Player) pj.getShooter();
		} else if(!(e.getDamager() instanceof Player)) return;
		if(p == null) {
			p = (Player) e.getDamager();
		}
		Player target = (Player) e.getEntity();
		GamePlayer tp = api.getPlayerManager().getGamePlayer(target);
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(tp.isWatchMode() || gp.isWatchMode()) return;
		tp.setLastHitTime(p.getName(), System.currentTimeMillis());
	}

}
