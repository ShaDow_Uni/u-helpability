package shadowuni.plugin.helpability.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.category.GameState;
import shadowuni.plugin.helpability.events.RangeOutEvent;
import shadowuni.plugin.helpability.objects.GameMap;
import shadowuni.plugin.helpability.objects.GamePlayer;
import shadowuni.plugin.helpability.objects.Party;

public class ControlListener implements Listener {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@SuppressWarnings({ "deprecation" })
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(gp.isDead()) {
			api.msg(p, ChatColor.RED, "게임에서 탈락하여 블럭 조작이 불가능합니다!");
			e.setCancelled(true);
			return;
		}
		if(!api.isUseBlockProtect() || api.getGameManager().isGameStarted()) return;
		if(api.isAdmin(p, false)) return;
		e.setCancelled(true);
		p.updateInventory();
		api.msg(p, ChatColor.RED, "게임 시작 전에는 블럭 조작이 불가능합니다!");
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(gp.isDead()) {
			api.msg(p, ChatColor.RED, "게임에서 탈락하여 블럭 조작이 불가능합니다!");
			e.setCancelled(true);
			return;
		}
		if(!api.isUseBlockProtect() || api.getGameManager().isGameStarted()) return;
		if(api.isAdmin(p, false)) return;
		e.setCancelled(true);
		api.msg(p, ChatColor.RED, "게임 시작 전에는 블럭 조작이 불가능합니다!");
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if(!api.isUseBlockProtect() || api.getGameManager().isGameStarted() || e.getClickedBlock() == null || e.getClickedBlock().getTypeId() != 36) return;
		e.setCancelled(true);
	}
	
	@SuppressWarnings({ "deprecation" })
	@EventHandler
	public void onBucketFill(PlayerBucketFillEvent e) {
		Player p = e.getPlayer();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(gp.isDead()) {
			api.msg(p, ChatColor.RED, "게임에서 탈락하여 블럭 조작이 불가능합니다!");
			e.setCancelled(true);
			return;
		}
		if(!api.isUseBlockProtect() || api.getGameManager().isGameStarted()) return;
		if(api.isAdmin(p, false)) return;
		e.setCancelled(true);
		p.updateInventory();
		api.msg(p, ChatColor.RED, "게임 시작 전에는 블럭 조작이 불가능합니다!");
	}
	
	@SuppressWarnings({ "deprecation" })
	@EventHandler
	public void onBucketEmpty(PlayerBucketEmptyEvent e) {
		Player p = e.getPlayer();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(gp.isDead()) {
			api.msg(p, ChatColor.RED, "게임에서 탈락하여 블럭 조작이 불가능합니다!");
			e.setCancelled(true);
			return;
		}
		if(!api.isUseBlockProtect() || api.getGameManager().isGameStarted()) return;
		if(api.isAdmin(p, false)) return;
		e.setCancelled(true);
		p.updateInventory();
		api.msg(p, ChatColor.RED, "게임 시작 전에는 블럭 조작이 불가능합니다!");
	}
	
	@EventHandler
	public void onPvp(EntityDamageByEntityEvent e) {
		if(!(e.getEntity() instanceof Player)) return;
		Player p = null;
		Player target = (Player) e.getEntity();
		GamePlayer tp = api.getPlayerManager().getGamePlayer(target);
		if(tp.isWatchMode()) return;
		else if(e.getDamager() instanceof Projectile) {
			Projectile pj = (Projectile) e.getDamager();
			if(!(pj.getShooter() instanceof Player)) return;
			p = (Player) pj.getShooter();
		} else if(!(e.getDamager() instanceof Player)) return;
		if(p == null) {
			p = (Player) e.getDamager();
		}
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(gp.hasParty()) {
			Party party = api.getPartyManager().getParty(gp.getPartyName());
			if(party.getPlayers().contains(target)) {
				api.msg(p, ChatColor.RED, "파티 중인 플레이어는 공격할 수 없습니다!");
				e.setCancelled(true);
				return;
			}
		} else if(gp.isDead()) {
			api.msg(p, ChatColor.RED, "게임에서 탈락하여 공격할 수 없습니다!");
			e.setCancelled(true);
			return;
		} else if(tp.isDead()) {
			api.msg(p, ChatColor.RED, "탈락한 플레이어는 공격할 수 없습니다!");
			e.setCancelled(true);
			return;
		}
		if(!api.isUsePvpProtect() || api.getGameManager().getGameState() == GameState.PLAYING || api.getGameManager().getGameState() == GameState.END) return;
		e.setCancelled(true);
		api.msg(p, ChatColor.RED, "게임 시작 전에는 PVP가 불가능합니다!");
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if(!(e.getEntity() instanceof Player)) return;
		Player p = (Player) e.getEntity();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(!api.isUseInvincbility() || api.getGameManager().getGameState() == GameState.PLAYING || api.getGameManager().getGameState() == GameState.END && !gp.isDead()) return;
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		Player p = e.getPlayer();
		if(api.isAdmin(p, false)) return;
		if(!api.isUseCmdProtect() || api.getGameManager().isGameStarted()) return;
		String[] cmd = e.getMessage().split(" ");
		for(String ec : api.getExceptionCommands()) {
			String[] ecmd = ec.split(" ");
			if(cmd[0].equals("/" + ecmd[0])) return;
		}
		api.msg(p, ChatColor.RED, "게임 시작 전에는 명령어 사용이 불가능합니다!");
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		if(!api.getGameManager().isGameStarted() || api.getMapManager().getPlayingMap() == null) return;
		GameMap playingMap = api.getMapManager().getPlayingMap();
		if(!api.getGameManager().isTeleportedAll()) {
			if(api.isUseAutoMapLimit()) {
				Location l = api.getGameManager().isTeleportedAll() ? playingMap.getTPAllLocation() : playingMap.getMapLocation();
				int limitRange = api.getGameManager().isTeleportedAll() ? api.getAutoTPAllMapLimitRange() : api.getAutoMapLimitRange();
				if(Math.pow(l.getX() - e.getTo().getX(), 2) + Math.pow(l.getZ() - e.getTo().getZ(), 2) <= Math.pow(limitRange, 2)) return;
				RangeOutEvent event = new RangeOutEvent(e.getPlayer(), playingMap);
				Bukkit.getPluginManager().callEvent(event);
				if(event.isCancelled()) return;
				e.setTo(isInMap(playingMap, e.getFrom()) ? e.getFrom() : playingMap.getMapLocation());
			} else if(playingMap.getMinMapLocation() != null && playingMap.getMaxMapLocation() != null) {
				Player p = e.getPlayer();
				if(p.getLocation().getY() < 1) return;
				if(!isInMap(playingMap, p.getLocation())) {
					RangeOutEvent event = new RangeOutEvent(e.getPlayer(), playingMap);
					Bukkit.getPluginManager().callEvent(event);
					if(event.isCancelled()) return;
					e.setTo(isInMap(playingMap, e.getFrom()) ? e.getFrom() : playingMap.getMapLocation());
				}
			}
			return;
		} 
	}
	
	@EventHandler
	public void onTeleport(PlayerTeleportEvent e) {
		Player p = e.getPlayer();
		for(GamePlayer asp : api.getPlayerManager().getGamePlayers()) {
			if(asp.getPlayer() == null || asp.isWatchMode()) continue;
			p.showPlayer(asp.getPlayer());
		}
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(gp == null || gp.isWatchMode()) return;
		for(Player ap : Bukkit.getOnlinePlayers()) {
			ap.showPlayer(p);
		}
	}
	
	private boolean isInMap(GameMap map, Location l) {
		if(l.getY() < 1) return true;
		Location min = map.getMinMapLocation();
		Location max = map.getMaxMapLocation();
		return map.getMapLocation().getWorld().equals(l.getWorld()) && min.getX() <= l.getX() && min.getZ() <= l.getZ() && max.getX() >= l.getX() && max.getZ() >= l.getZ();
	}

}
