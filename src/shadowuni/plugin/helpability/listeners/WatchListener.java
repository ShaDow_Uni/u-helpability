package shadowuni.plugin.helpability.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.events.JoinEvent;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class WatchListener implements Listener {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@EventHandler
	public void onJoin(JoinEvent e) {
		GamePlayer gp = e.getGamePlayer();
		Player p = gp.getPlayer();
		for(Player ap : Bukkit.getOnlinePlayers()) {
			if(api.getPlayerManager().getGamePlayer(ap).isHide()) {
				p.hidePlayer(ap);
				continue;
			}
			p.showPlayer(ap);
		}
		if(gp.isHide()) {
			for(Player ap : Bukkit.getOnlinePlayers()) {
				ap.hidePlayer(p);
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(!gp.isWatchMode()) return;
		e.setCancelled(true);
		p.updateInventory();
		api.msg(p, ChatColor.RED, "관전 중에는 블럭 조작이 불가능합니다!");
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(!gp.isWatchMode()) return;
		e.setCancelled(true);
		p.updateInventory();
		api.msg(p, ChatColor.RED, "관전 중에는 블럭 조작이 불가능합니다!");
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBucketEmpty(PlayerBucketEmptyEvent e) {
		Player p = e.getPlayer();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(!gp.isWatchMode()) return;
		e.setCancelled(true);
		p.updateInventory();
		api.msg(p, ChatColor.RED, "관전 중에는 블럭 조작이 불가능합니다!");
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBucketFill(PlayerBucketFillEvent e) {
		Player p = e.getPlayer();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(!gp.isWatchMode()) return;
		e.setCancelled(true);
		p.updateInventory();
		api.msg(p, ChatColor.RED, "관전 중에는 블럭 조작이 불가능합니다!");
	}
	
	@EventHandler
	public void onPvp(EntityDamageByEntityEvent e) {
		if(!(e.getEntity() instanceof Player) || !(e.getDamager() instanceof Player)) return;
		Player p = (Player) e.getDamager();
		if(!api.getPlayerManager().getGamePlayer(p).isWatchMode()) return;
		e.setCancelled(true);
		api.msg(p, ChatColor.RED, "관전 중에는 PVP가 불가능합니다!");
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if(!(e.getEntity() instanceof Player)) return;
		if(!api.getPlayerManager().getGamePlayer((Player) e.getEntity()).isWatchMode()) return;
		e.setCancelled(true);
	}
	
	@EventHandler (priority=EventPriority.LOWEST)
	public void onGamePlayerInteractWatchPlayer(PlayerInteractEvent e) {
		GamePlayer gp = api.getPlayerManager().getGamePlayer(e.getPlayer());
		if(e.getClickedBlock() == null || gp.isDead() || gp.isWatchMode()) return;
		for(GamePlayer wp : api.getPlayerManager().getWatchPlayers()) {
			if(wp.getPlayer() == null || !isOverLap(e.getClickedBlock().getLocation(), wp.getPlayer().getLocation())) continue;
			wp.getPlayer().teleport(getEmptyLocation(e.getPlayer().getLocation()));
			api.msg(wp.getPlayer(), ChatColor.RED, "게임 중인 플레이어에게 방해되어 강제로 텔레포트되었습니다.");
		}
	}
	
	@EventHandler (priority=EventPriority.LOWEST)
	public void onInteract(PlayerInteractEvent e) {
		if(!(e.getPlayer() instanceof Player)) return;
		if(!api.getPlayerManager().getGamePlayer((Player) e.getPlayer()).isWatchMode()) return;
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onDropItem(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(!gp.isWatchMode()) return;
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onPickupItem(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(!gp.isWatchMode()) return;
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		Player p = e.getPlayer();
		if(api.isAdmin(p, false)) return;
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(gp == null || !gp.isWatchMode() || !api.isUseWatchCmdProtect() || !api.getGameManager().isGameStarted()) return;
		String[] cmd = e.getMessage().split(" ");
		for(String ec : api.getExceptionWatchCommands()) {
			String[] ecmd = ec.split(" ");
			if(cmd[0].equals("/" + ecmd[0])) return;
		}
		api.msg(p, ChatColor.RED, "관전 중에 사용이 불가능한 명령어입니다!");
		e.setCancelled(true);
	}
	
	private boolean isOverLap(Location block, Location player) {
		return Math.pow(block.getY() - player.getY(), 2) <= 4 && Math.pow(block.getX() - player.getX(), 2) < Math.pow(1.3, 2) && Math.pow(block.getZ() - player.getZ(), 2) < Math.pow(1.3, 2);
	}
	
	private Location getEmptyLocation(Location loc) {
		List<Location> locs = new ArrayList<>();
		for(int i = 1; i < 3; i++) {
			for(int j = 1; j < 3; j++) {
				if(loc.getWorld().getBlockTypeIdAt((int) loc.getX() + i, (int) loc.getY(), (int) loc.getZ() + j) == 0 && loc.getWorld().getBlockTypeIdAt((int) loc.getX() + i, (int) loc.getY() + 1, (int) loc.getZ() + j) == 0) {
					locs.add(new Location(loc.getWorld(), loc.getX() + i, loc.getY(), loc.getZ() + j, loc.getYaw(), loc.getPitch()));
				}
			}
		}
		if(locs.size() == 0) {
			locs.add(loc);
		}
		if(locs.size() < 1) return api.getGameManager().isGameStarted() ? (api.getGameManager().isTeleportedAll() ? api.getMapManager().getPlayingMap().getTPAllLocation() : api.getMapManager().getPlayingMap().getMapLocation()) : api.getMapManager().getSpawnMap().getMapLocation();
		return locs.get(api.getRandomRange(0, locs.size() - 1));
	}
	
}
