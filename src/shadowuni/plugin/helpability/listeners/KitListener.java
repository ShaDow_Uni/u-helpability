package shadowuni.plugin.helpability.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;

public class KitListener implements Listener {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		Player p = (Player) e.getPlayer();
		if(!api.isAdmin(p, false)) return;
		Inventory inv = e.getInventory();
		if(!api.getKitManager().existKit(inv.getName())) return;
		api.getKitManager().setKit(inv.getName(), inv);
		api.getFileManager().saveKit(inv);
		api.msg(p, ChatColor.BLUE, "'" + inv.getName() + "' 킷을 저장했습니다!");
	}

}