package shadowuni.plugin.helpability.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.category.GameState;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class AutoGameListener implements Listener {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@EventHandler (priority = EventPriority.HIGH)
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		String msg = gp.getDisplayName() + "님께서 접속하셨습니다.";
		ChatColor color = ChatColor.YELLOW;
		if(!api.getGameManager().isGameStarted()) {
			msg += " ( " + (api.getPlayerManager().getGamePlayers().size()) + " / " + api.getStartPlayerCount() + " )";
			api.getBarApi().setMessage(msg, (float) api.getPlayerManager().getGamePlayers().size() / (float) api.getStartPlayerCount() * 100);
			if(api.getStartPlayerCount() <= api.getPlayerManager().getGamePlayers().size() && !(api.isUseBungeeParty() && api.getPartyApi().getPartyManager().getPartys().size() == 1 && api.getPartyApi().getPartyManager().getPartys().values().iterator().next().getOnlinePlayers().size() == api.getPlayerManager().getGamePlayers().size())) {
				api.getGameManager().GameStart(null);
			}
		} else if(!gp.isDead() && !gp.isWatchMode()) {
			msg = gp.getDisplayName() + "님께서 재접속하셨습니다.";
		} else if(gp.isWatchMode()) {
			color = ChatColor.BLUE;
			msg = gp.getDisplayName() + "님께서 관전 모드로 접속하셨습니다! (관전자 수: " + api.getPlayerManager().getWatchPlayers().size() + "명)";
		}
		e.setJoinMessage(api.buildmsg(color, msg));
	}
	
	@EventHandler (priority = EventPriority.HIGH)
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		String msg = gp.getDisplayName() + "님께서 퇴장하셨습니다.";
		if(api.getGameManager().isGameStarted()) {
			if(api.getGameManager().finish()) {
				api.shutdown(13);
			} else if(!api.getGameManager().getGameState().equals(GameState.END) && api.getPlayerManager().getGamePlayers().size() < 2) {
				for(Player ap : Bukkit.getOnlinePlayers()) {
					ap.teleport(api.getMapManager().getSpawnMap().getMapLocation());
				}
				api.getGameManager().GameStop(null);
				api.broadcast(ChatColor.DARK_GREEN, "인원 수가 부족하여 게임이 중단됩니다!");
			}
		} else {
			msg += " ( " + api.getPlayerManager().getGamePlayers().size() + " / " + api.getStartPlayerCount() + " )";
			api.getBarApi().setMessage(msg, (float) api.getPlayerManager().getGamePlayers().size() / (float) api.getStartPlayerCount() * 100);
			if(api.getStartPlayerCount() <= api.getPlayerManager().getGamePlayers().size() - 1) {
				api.getGameManager().GameStart(null);
			}
		}
		gp.setQuitTime(System.currentTimeMillis());
		e.setQuitMessage(api.buildmsg(ChatColor.YELLOW, msg));
	}
	
}
