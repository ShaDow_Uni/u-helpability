package shadowuni.plugin.helpability.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import Xeon.VisualAbility.MainModule.AbilityBase;
import Xeon.VisualAbility.MajorModule.AbilityList;
import shadowuni.plugin.bungeeparty.objects.PartyPlayer;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.category.GameState;
import shadowuni.plugin.helpability.events.DeathEvent;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class BungeePartyListener implements Listener {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@EventHandler
	public void onDeath(DeathEvent e) {
		Player p = e.getPlayer();
		PartyPlayer pp = api.getPartyApi().getPlayerManager().getPartyPlayer(p);
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(pp == null || !pp.hasParty() || gp == null || !gp.isDead() || gp.isWatchMode()) return;
		else if(api.isUseBitAbility()) {
			for(AbilityBase ab : AbilityList.AbilityList) {
				if(!ab.PlayerCheck(p)) continue;
				ab.SetPlayer(null, false);
			}
		} else if(api.isUseDyeAbility()) {
			for(Physical.Fighters.MainModule.AbilityBase ab : Physical.Fighters.MajorModule.AbilityList.AbilityList) {
				if(!ab.PlayerCheck(p)) continue;
				ab.SetPlayer(null, false);
			}
		}
		gp.setWatchMode(true);
		gp.hidePlayer();
		p.setGameMode(GameMode.ADVENTURE);
		p.setAllowFlight(true);
		p.setFlying(true);
		api.broadcast(ChatColor.BLUE, "'" + gp.getDisplayName() + "' 님께서 탈락하여 관전 모드로 전환되었습니다. (관전자 수: " + api.getPlayerManager().getWatchPlayers().size() + "명)");
		if(api.getGameManager().finish()) {
			api.shutdown(13);
		} else if(!api.getGameManager().getGameState().equals(GameState.END) && api.getPlayerManager().getGamePlayers().size() < 2) {
			for(Player ap : Bukkit.getOnlinePlayers()) {
				ap.teleport(api.getMapManager().getSpawnMap().getMapLocation());
			}
			api.getGameManager().GameStop(null);
			api.broadcast(ChatColor.DARK_GREEN, "인원 수가 부족하여 게임이 중단됩니다!");
		}
	}
	
}