package shadowuni.plugin.helpability.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import shadowuni.plugin.bungeeparty.objects.PartyPlayer;
import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;
import shadowuni.plugin.helpability.objects.GamePlayer;

public class BungeeListener implements Listener {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@SuppressWarnings({ "static-access", "deprecation" })
	@EventHandler (priority = EventPriority.HIGHEST)
	public void onDeath(PlayerDeathEvent e) {
		if(!api.isUseBungeecord() || !api.getGameManager().isGameStarted()) return;
		final Player p = e.getEntity();
		if(api.isUseBungeeParty()) {
			PartyPlayer pp = api.getPartyApi().getPlayerManager().getPartyPlayer(p);
			if(pp != null && pp.hasParty()) return;
		}
		GamePlayer gp = api.getPlayerManager().getGamePlayer(p);
		if(!gp.isDead() || gp.isWatchMode()) return;
		if(api.isUseBungeeChat() && api.getChatApi().uselobby) {
			api.getChatApi().getLobbyManager().sendLobby(p);
		} else {
			api.getBungeeManager().sendTo(p, api.getLobbyServer());
		}
		api.msg(p, ChatColor.AQUA, "로비로 이동됩니다.");
		final String name = p.getName();
		Bukkit.getScheduler().scheduleAsyncDelayedTask(HelpAbility.getInstance(), () ->
		api.getBungeeManager().sendMessage(name, api.buildmsg(ChatColor.RED, "패배하셨습니다. 다음 기회를 노려보세요!")), 10);
		Bukkit.getScheduler().scheduleAsyncDelayedTask(HelpAbility.getInstance(), () -> { if(p.isOnline()) p.kickPlayer("로비로 이동할 수 없어 퇴장되었습니다."); }, 100);
	}
	
}
