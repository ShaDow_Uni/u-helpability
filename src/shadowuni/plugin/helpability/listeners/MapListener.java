package shadowuni.plugin.helpability.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import shadowuni.plugin.helpability.HelpAbility;
import shadowuni.plugin.helpability.HelpAbilityAPI;

public class MapListener implements Listener {
	
	private HelpAbilityAPI api = HelpAbility.getApi();
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if(!api.isAdmin(p, false)) return;
		Action action = e.getAction();
		if(!(action.equals(Action.LEFT_CLICK_BLOCK) || action.equals(Action.RIGHT_CLICK_BLOCK))) return;
		ItemStack hand = p.getItemInHand();
		if(hand == null || !hand.getType().equals(Material.WOOD_AXE)) return;
		Location location = e.getClickedBlock().getLocation();
		if(action.equals(Action.LEFT_CLICK_BLOCK)) {
			api.getMapManager().setLeftLocation(p, location);
			e.setCancelled(true);
			api.msg(p, ChatColor.BLUE, "위치 1을 (X: " + location.getX() + ", Y: " + location.getY() + ", Z: " + location.getZ() + ") 로 설정했습니다.");
			return;
		} else if(action.equals(Action.RIGHT_CLICK_BLOCK)) {
			api.getMapManager().setRightLocation(p, location);
			api.msg(p, ChatColor.BLUE, "위치 2를 (X: " + location.getX() + ", Y: " + location.getY() + ", Z: " + location.getZ() + ") 로 설정했습니다.");
			return;
		}
	}

}
