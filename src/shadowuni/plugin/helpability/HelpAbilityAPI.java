package shadowuni.plugin.helpability;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_5_R3.entity.CraftPlayer;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;

import lombok.Getter;
import lombok.Setter;
import me.confuser.barapi.BarAPI;
import net.minecraft.server.v1_5_R3.Packet63WorldParticles;
import shadowuni.plugin.bungeechat.BungeeChatAPI;
import shadowuni.plugin.bungeeparty.BungeePartyAPI;
import shadowuni.plugin.helpability.managers.BungeeManager;
import shadowuni.plugin.helpability.managers.FileManager;
import shadowuni.plugin.helpability.managers.GameManager;
import shadowuni.plugin.helpability.managers.KitManager;
import shadowuni.plugin.helpability.managers.MapManager;
import shadowuni.plugin.helpability.managers.PartyManager;
import shadowuni.plugin.helpability.managers.PlayerManager;
import shadowuni.plugin.helpability.managers.RankItemManager;
import shadowuni.plugin.helpability.managers.SupplyManager;
import shadowuni.plugin.helpability.managers.TaskManager;
import shadowuni.plugin.helpability.managers.VoteManager;
import shadowuni.plugin.helpability.objects.GamePlayer;
import shadowuni.plugin.library.LibraryPlugin;
import shadowuni.plugin.library.api.LibraryAPI;
import shadowuni.plugin.prefixer.PrefixerAPI;

public class HelpAbilityAPI {
	
	@Getter
	private final boolean test = false;
	
	protected final String pluginPrefix = ChatColor.GREEN + "[ U-HelpAbility ] " + ChatColor.WHITE;
	
	@Setter
	@Getter
	protected String lobbyServer, serverName, waitMotd, gameMotd, mapLimitParticle, partyParticle;
	@Setter
	@Getter
	protected String[] bungeePlayerList;
	
	@Setter
	@Getter
	protected int startPlayerCount, skipCount, TPAllCount, TPAllRepeatCount, winMinCount, voteCount, maxPartyPlayerCount, assistCount, locationNotifyInterval,
	doubleCount, tripleCount, quadraCount, pentaCount, supplyCreateInterval, reconnectTime, quitDeathHealth, autoMapLimitRange, autoTPAllMapLimitRange;
	
	@Setter
	@Getter
	protected double killMoney, winMoney, assistMoney, firstMoney, doubleMoney, tripleMoney, quadraMoney, pentaMoney;
	
	@Setter
	@Getter
	protected boolean useParty, useSupplyFirework, useBitAbility, useDyeAbility, useBungeeChat, usePrefixer, useBungeeParty;
	@Setter
	@Getter
	protected boolean useBar, useRankItem, useTPAll, useTPAllRepeat, useInvincbility, usePvpProtect, useBlockProtect, useCmdProtect, useWatchCmdProtect, useBungeecord, allowNatureDeath,
	useSupply, allowReconnect, useAutoMapLimit, useAutoTPAllMapLimit, useMapLimitParticle, usePartyParticle, useAssist, useFirst, useDouble, useTriple, useQuadra, usePenta, useLocationNotifyMessage, useLocationNotifyFirework,
	firstBlood = true;
	
	@Setter
	@Getter
	protected List<String> exceptionCommands = new ArrayList<>();
	@Setter
	@Getter
	protected List<String> exceptionWatchCommands = new ArrayList<>();
	
	@Getter
	private LibraryAPI library = LibraryPlugin.getApi();
	@Getter
	protected BungeeChatAPI chatApi;
	@Getter
	protected BungeePartyAPI partyApi;
	@Getter
	protected PrefixerAPI prefixerApi;
	@Getter
	private BarAPI barApi;
	@Getter
	private GameManager gameManager;
	@Getter
	private MapManager mapManager;
	@Getter
	private PartyManager partyManager;
	@Getter
	private PlayerManager playerManager;
	@Getter
	private KitManager kitManager;
	@Getter
	private RankItemManager rankItemManager;
	@Getter
	private SupplyManager supplyManager;
	@Getter
	private VoteManager voteManager;
	@Getter
	private TaskManager taskManager;
	@Getter
	private BungeeManager bungeeManager;
	@Getter
	private FileManager fileManager;
	
	public void init() {
		barApi = new BarAPI();
		gameManager = new GameManager();
		mapManager = new MapManager();
		partyManager = new PartyManager();
		playerManager = new PlayerManager();
		kitManager = new KitManager();
		rankItemManager = new RankItemManager();
		supplyManager = new SupplyManager();
		voteManager = new VoteManager();
		taskManager = new TaskManager();
		bungeeManager = new BungeeManager();
		fileManager = new FileManager();
	}
	
	public void log(String log) {
		Bukkit.getConsoleSender().sendMessage(pluginPrefix + log);
	}
	
	public void msg(Player p, ChatColor color, String msg) {
		p.sendMessage(buildmsg(color, msg));
	}
	
	public void nmsg(Player p, String msg) {
		p.sendMessage(msg);
	}
	
	public String buildmsg(ChatColor color, String msg) {
		return color + "◆ [ " + ChatColor.WHITE + msg + color+ " ]";
	}
	
	public void broadcast(ChatColor color, String msg) {
		Bukkit.broadcastMessage(buildmsg(color, msg));
	}
	
	public boolean isAdmin(Player p, boolean msg) {
		if(p.hasPermission("helpability.admin") || p.isOp()) {
			return true;
		} else if(msg) {
			msg(p, ChatColor.RED, "권한이 없습니다!");
		}
		return false;
	}
	
	public String buildTime(int time) {
		if(time > 3600) {
			return (time - (time % 3600)) / 3600 + "시간 " + (time % 3600 - (time % 60)) / 60 + "분 " + time % 60 + "초";
		} else if(time >= 3600 && time % 3600 == 0) {
			return time / 3600 + "시간";
		} else if(time > 60) {
			return (time - (time % 60)) / 60 + "분 " + time % 60 + "초";
		} else if(time >= 60 && time % 60 == 0) {
			return time / 60 + "분";
		}
		return time + "초";
	}
	
	public String buildStringList(List<String> strings) {
		StringBuilder sb = new StringBuilder();
		for(String s : strings) {
			if(sb.length() < 1) {
				sb.append(s);
			} else {
				sb.append(", " + s);
			}
		}
		return sb.toString();
	}
	
	public String buildStringList(Set<String> set) {
		StringBuilder sb = new StringBuilder();
		for(String s : set) {
			if(sb.length() < 1) {
				sb.append(s);
			} else {
				sb.append(", " + s);
			}
		}
		return sb.toString();
	}
	
	public String buildGamePlayerList(List<GamePlayer> players) {
		StringBuilder sb = new StringBuilder();
		if(players.size() < 1) {
			return "없음";
		}
		for(GamePlayer p : players) {
			if(sb.length() < 1) {
				sb.append(p.getName());
			} else {
				sb.append(", " + p.getName());
			}
		}
		return sb.toString();
	}
	
	public String buildPlayerList(List<Player> players) {
		StringBuilder sb = new StringBuilder();
		if(players.size() < 1) {
			return "없음";
		}
		for(Player p : players) {
			if(sb.length() < 1) {
				sb.append(p.getName());
			} else {
				sb.append(", " + p.getName());
			}
		}
		return sb.toString();
	}
	
	public String buildItemListString(List<ItemStack> itemlist) {
		StringBuilder sb = new StringBuilder();
		if(itemlist.size() < 1) {
			return "없음";
		}
		for(ItemStack item : itemlist) {
			String info = item.getType().toString() + " x " + item.getAmount();
			if(sb.length() < 1) {
				sb.append(info);
			} else {
				sb.append(", " + info);
			}
		}
		return sb.toString();
	}
	
	public String replaceColor(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	public int getRandomRange(int n1, int n2) {
		return (int) (Math.random() * (n2 - n1 + 1)) + n1; 
	}
	
	public void spawnParticles(String particle, Location location, float offsetx, float offsety, float offsetz, float speed, int count) {
		spawnParticles(particle, (float) location.getX(), (float) location.getY(), (float) location.getZ(), offsetx, offsety, offsetz, speed, count);
	}
	
	public void spawnParticles(Player p, String particle, Location location, float offsetx, float offsety, float offsetz, float speed, int count) {
		spawnParticles(p, particle, (float) location.getX(), (float) location.getY(), (float) location.getZ(), offsetx, offsety, offsetz, speed, count);
	}
	
	public void spawnParticles(String particle, float x, float y, float z, float offsetx, float offsety, float offsetz, float speed, int count) {
		for(Player ap : Bukkit.getOnlinePlayers()) {
			spawnParticles(ap, particle, x, y, z, offsetx, offsety, offsetz, speed, count);
		}
	}
	
	public void spawnParticles(Player p, String particle, float x, float y, float z, float offsetx, float offsety, float offsetz, float speed, int count) {
		Packet63WorldParticles packet = new Packet63WorldParticles();
		for(Field field : packet.getClass().getDeclaredFields()) {
			try{
				field.setAccessible(true);
				switch(field.getName()) {
				case "a":
					field.set(packet, particle);
					break;
				case "b":
					field.setFloat(packet, x);
					break;
				case "c":
					field.setFloat(packet, y);
					break;
				case "d":
					field.setFloat(packet, z);
					break;
				case "e":
					field.setFloat(packet, offsetx);
					break;
				case "f":
					field.setFloat(packet, offsety);
					break;
				case "g":
					field.setFloat(packet, offsetz);
					break;
				case "h":
					field.setFloat(packet, speed);
					break;
				case "i":
					field.setInt(packet, count);
					break;
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		((CraftPlayer)p).getHandle().playerConnection.sendPacket(packet);
	}
	
	public void spawnFirework(Location location) {
		Firework fw = location.getWorld().spawn(location, Firework.class);
		FireworkMeta fm = fw.getFireworkMeta();
		FireworkEffect fe = FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.BLUE).withFade(Color.AQUA).build();
		fm.setPower(3);
		fm.addEffect(fe);
		fw.setFireworkMeta(fm);
	}
	
	@SuppressWarnings("deprecation")
	public void shutdown(int count) {
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(HelpAbility.getInstance(), new Runnable() {
			int i = 0;
			@SuppressWarnings("static-access")
			public void run() {
				i++;
				if(i == (count - 3) && useBungeecord) {
					for(Player ap : Bukkit.getOnlinePlayers()) {
						if(isUseBungeeChat() && getChatApi().uselobby) {
							getChatApi().getLobbyManager().sendLobby(ap);
						} else {
							getBungeeManager().sendTo(ap, getLobbyServer());
						}
					}
				} else if(i == count) {
					Bukkit.shutdown();
				}
			}
		}, 0, 20);
	}

}